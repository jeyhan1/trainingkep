package kookmin.uni.sugang.professor.domain.spec.lifecycle.pojo;

import kookmin.uni.sugang.professor.domain.spec.crud.ProfessorService;
import kookmin.uni.sugang.professor.domain.spec.lifecycle.RegistrationServiceLycler;
import kookmin.uni.sugang.professor.domain.store.lifecycle.RegistrationStoreLycler;
import kookmin.uni.sugang.professor.domain.store.map.RegistrationMapStoreLycler;

public class RegistrationPojoServiceLycler implements RegistrationServiceLycler {
    //
    private static RegistrationServiceLycler singletonServiceLycler;

    private ProfessorService professorService;

    public RegistrationPojoServiceLycler() {
        //
        RegistrationStoreLycler storeLycler = RegistrationMapStoreLycler.getInstance();
    }

    public static RegistrationServiceLycler getInstance() {
        //
        if(singletonServiceLycler == null) {
            singletonServiceLycler = new RegistrationPojoServiceLycler();
        }

        return singletonServiceLycler;
    }

    public static RegistrationServiceLycler newInstance() {
        //
        return new RegistrationPojoServiceLycler();
    }

    @Override
    public ProfessorService requestProfessorService() {
        return professorService;
    }

 }
