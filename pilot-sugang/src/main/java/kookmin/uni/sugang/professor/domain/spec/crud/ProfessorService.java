package kookmin.uni.sugang.professor.domain.spec.crud;

import io.naraplatform.share.domain.NameValueList;
import kookmin.uni.sugang.professor.domain.entity.Professor;
import kookmin.uni.sugang.professor.domain.spec.crud.sdo.ProfessorCdo;

public interface ProfessorService {
    String registrationProfessor(ProfessorCdo professroCdo);
    Professor findProfessor(String professorId);
    void modifyProfessor(String professorId, NameValueList nameValueList);
    void removeProfessor(String professorId);
}
