package kookmin.uni.sugang.professor.domain.store;

import kookmin.uni.sugang.professor.domain.entity.Professor;

public interface ProfessorStore {
    void create(Professor professor);
    Professor retrieve(String id);
    void update(Professor professor);
    void delete(Professor professor);
    boolean existsByName(String name);
}
