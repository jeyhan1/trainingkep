package kookmin.uni.sugang.professor.domain.spec.crud.sdo;

import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import kookmin.uni.sugang.professor.domain.entity.Professor;
import kookmin.uni.sugang.professor.domain.entity.ProfessorStateType;
import kookmin.uni.sugang.professor.domain.entity.ProfessorType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProfessorCdo implements JsonSerializable {
    //
    private String professorNo;
    private String name;
    private String departmentId;
    private ProfessorType professorType;
    private ProfessorStateType professorStateType;

    public ProfessorCdo(String professorNo,
                     String name,
                     String departmentId) {
        super();
        this.professorNo = professorNo;
        this.name = name;
        this.departmentId = departmentId;
    }

    public String toString() {
        return toJson();
    }

    public static Professor fromJson(String json) {
        return JsonUtil.fromJson(json, Professor.class);
    }

    public static ProfessorCdo sample() {

        Professor professor = Professor.sample();
        return new ProfessorCdo(
                professor.getProfessorNo(),
                professor.getName(),
                professor.getDepartmentId()
        );
    }

    public static void main(String[] args) {
        System.out.println(sample());
    }
}
