package kookmin.uni.sugang.professor.domain.spec.lifecycle;

import kookmin.uni.sugang.professor.domain.spec.crud.ProfessorService;

public interface RegistrationServiceLycler {
    //
    ProfessorService requestProfessorService();
}
