package kookmin.uni.sugang.professor.domain.store.map;

import kookmin.uni.sugang.professor.domain.store.ProfessorStore;
import kookmin.uni.sugang.professor.domain.store.lifecycle.RegistrationStoreLycler;

public class RegistrationMapStoreLycler implements RegistrationStoreLycler {
    //
    private static RegistrationStoreLycler singletonStoreLycler;

    private ProfessorStore professorStore;

    protected RegistrationMapStoreLycler() {
        //
        this.professorStore = new ProfessorMapStore();
    }

    public static RegistrationStoreLycler getInstance() {
        //
        if(singletonStoreLycler == null) {
            singletonStoreLycler = new RegistrationMapStoreLycler();
        }

        return singletonStoreLycler;
    }

    public static RegistrationStoreLycler newInstance() {
        //
        return new RegistrationMapStoreLycler();
    }

    @Override
    public ProfessorStore requestProfessorStore() {
        return professorStore;
    }
}
