package kookmin.uni.sugang.professor.domain.entity;

import io.naraplatform.share.domain.NameValue;
import io.naraplatform.share.domain.NameValueList;
import io.naraplatform.share.domain.nara.NaraEntity;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Professor extends NaraEntity {
    //
    private String professorNo;
    private String name;
    private String departmentId;
    private ProfessorType professorType;
    private ProfessorStateType professorStateType;

    public Professor(String professorNo,
                     String name,
                     String departmentId) {
        //
        super(professorNo);
        this.professorNo = professorNo;
        this.name = name;
        this.departmentId = departmentId;
    }

    public String toString() {
        return toJson();
    }

    public static Professor fromJson(String json) {
        return JsonUtil.fromJson(json, Professor.class);
    }

    public void setValues(NameValueList nameValues) {
        //
        for(NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();
            switch (nameValue.getName()) {
                case "name":
                    this.name = value;
                    break;
                case "departmentId":
                    this.name = departmentId;
                    break;
                default:
                    throw new IllegalArgumentException("Update not allowed: " + nameValue);
            }
        }
    }

    public static Professor sample() {
        String professorNo = "ABCDE";
        String name = "홍길동";
        String departmentId = "A303";

        Professor sample = new Professor(professorNo, name, departmentId);
        sample.setProfessorType(ProfessorType.FullTimeProfessor);
        sample.setProfessorStateType(ProfessorStateType.Served);

        return sample;
    }

    public static void main(String[] args) {
        //
        //ProfessorType professorType = new ProfessorType("1");
        ProfessorType professorType = ProfessorType.valueOf("AssociateProfessor");
        System.out.println("Code: " + sample().getProfessorType().name());
        System.out.println(sample());
    }

}
