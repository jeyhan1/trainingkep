package kookmin.uni.sugang.professor.domain.store.lifecycle;

import kookmin.uni.sugang.professor.domain.store.ProfessorStore;

public interface RegistrationStoreLycler {
    //
    ProfessorStore requestProfessorStore();
}