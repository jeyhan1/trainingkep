package kookmin.uni.sugang.professor.domain.store.map;

import java.util.LinkedHashMap;
import java.util.Map;

import kookmin.uni.sugang.professor.domain.entity.Professor;
import kookmin.uni.sugang.professor.domain.store.ProfessorStore;

public class ProfessorMapStore implements ProfessorStore {
    private Map<String, Professor> professorMap;

    public ProfessorMapStore() {
        this.professorMap = new LinkedHashMap<>();
    }

    @Override
    public void create(Professor professor) {
        professorMap.put(professor.getId(), professor);
    }

    @Override
    public Professor retrieve(String id) {
        return professorMap.get(id);
    }

    @Override
    public void update(Professor professor) {
        // do noting
    }

    @Override
    public void delete(Professor professor) {
        professorMap.remove(professor.getId());
    }

    @Override
    public boolean existsByName(String name) {
        for(Professor professor : professorMap.values()) {
            if(professor.getName().equals(name)) {
                return true;
            }
        }

        return false;
    }
}
