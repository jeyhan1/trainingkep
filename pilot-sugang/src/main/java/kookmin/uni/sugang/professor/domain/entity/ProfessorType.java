package kookmin.uni.sugang.professor.domain.entity;

public enum ProfessorType {
    HonoraryProfessor("1"),
    AdjunctProfessor("2"),
    FullTimeProfessor("3"),
    AssociateProfessor("4"),
    AssistantProfessor("5"),
    Instructor("6");

    private String code;

    ProfessorType(String code) {
        //
        this.code = code;
    }

    public String code() {
        //
        return code;
    }

    public ProfessorType fromCode(String code) {
        //
        return null;
    }

    public boolean equalsFromCode(String code) {
        //
        return false;
    }
}
