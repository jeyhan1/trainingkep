package kookmin.uni.sugang.professor.domain.entity;

public enum ProfessorStateType {
    Served("1"),
    Leave("2"),
    retirement("3");

    private String code;

    ProfessorStateType(String code) {
        //
        this.code = code;
    }

    public String code() {
        //
        return code;
    }

    public ProfessorStateType fromCode(String code) {
        //
        return null;
    }

    public boolean equalsFromCode(String code) {
        //
        return false;
    }
}
