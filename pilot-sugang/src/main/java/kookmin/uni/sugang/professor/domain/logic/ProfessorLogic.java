package kookmin.uni.sugang.professor.domain.logic;

import java.util.NoSuchElementException;

import io.naraplatform.share.domain.NameValueList;
import io.naraplatform.share.exception.store.AlreadyExistsException;
import kookmin.uni.sugang.professor.domain.entity.Professor;
import kookmin.uni.sugang.professor.domain.spec.crud.ProfessorService;
import kookmin.uni.sugang.professor.domain.spec.crud.sdo.ProfessorCdo;
import kookmin.uni.sugang.professor.domain.store.ProfessorStore;
import kookmin.uni.sugang.professor.domain.store.lifecycle.RegistrationStoreLycler;

public class ProfessorLogic implements ProfessorService {
    private ProfessorStore professorStore;

    public ProfessorLogic(RegistrationStoreLycler storeLycler) {
        this.professorStore = storeLycler.requestProfessorStore();
    }

    @Override
    public String registrationProfessor(ProfessorCdo professorCdo) {
        String name = professorCdo.getName();
        if(professorStore.existsByName(name)) {
            throw new AlreadyExistsException("Professor Name: " + name);
        }

        Professor professor = new Professor(
                professorCdo.getProfessorNo(),
                professorCdo.getName(),
                professorCdo.getDepartmentId()
        );

        professorStore.create(professor);

        return professor.getId();
    }

    @Override
    public Professor findProfessor(String professorId) {
        Professor professor = professorStore.retrieve(professorId);
        if(professor == null) {
            throw new NoSuchElementException("Professor id: " + professorId);
        }
        return professor;
    }

    @Override
    public void modifyProfessor(String professorId, NameValueList nameValueList) {
        Professor professor = findProfessor(professorId);
        professor.setValues(nameValueList);

        professorStore.update(professor);
    }

    @Override
    public void removeProfessor(String professorId) {
        Professor professor = findProfessor(professorId);
        professorStore.delete(professor);
    }
}
