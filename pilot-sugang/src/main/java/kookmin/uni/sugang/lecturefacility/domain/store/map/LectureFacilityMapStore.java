package kookmin.uni.sugang.lecturefacility.domain.store.map;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import kookmin.uni.sugang.lecturefacility.domain.entity.Building;
import kookmin.uni.sugang.lecturefacility.domain.entity.Room;
import kookmin.uni.sugang.lecturefacility.domain.store.LectureFacilityStore;

public class LectureFacilityMapStore implements LectureFacilityStore {
    //
    private Map<String,Building> buildingMap;
    private Map<String, Room> roomMap;

    public LectureFacilityMapStore() {
        //
        this.buildingMap = new LinkedHashMap<>();
        this.roomMap = new LinkedHashMap<>();
    }

    @Override
    public void createBuilding(Building building) {

    }

    @Override
    public Building retrieveBuilding(String id) {
        return null;
    }

    @Override
    public List<Building> retrieveByNameLike(String nameLike) {
        return null;
    }

    @Override
    public List<Building> retrieveAll(int offset, int limit) {
        return null;
    }

    @Override
    public void updateBuilding(Building building) {

    }

    @Override
    public void deleteBuilding(Building building) {

    }

    @Override
    public boolean exists(String buildingId) {
        return false;
    }

    @Override
    public int count() {
        return 0;
    }

    @Override
    public void createRoom(Room room) {

    }

    @Override
    public Room retrieveRoom(String id) {
        return null;
    }

    @Override
    public void updateRoom(Room room) {

    }

    @Override
    public void deleteRoom(Room room) {

    }

    @Override
    public boolean existsRoomByName(String name) {
        return false;
    }

    @Override
    public int countRoomByBuildingId(String buildingId) {
        return 0;
    }

    //
}
