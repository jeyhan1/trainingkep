package kookmin.uni.sugang.lecturefacility.domain.entity.key;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class LectureRoomKey {
    //
    private String departmentId;
    private String buildingId;
    private String roomId;
    private String seatId;

    public BuildingKey genBuildingKey() {
        //
        return new BuildingKey(departmentId, buildingId);
    }

    public boolean partOf(BuildingKey buildingKey) {
        //
        if(this.departmentId.equals(buildingKey.getDepartmentId()) &&
                this.buildingId.equals(buildingKey.getBuildingId())) {
            return true;
        }

        return false;
    }
}
