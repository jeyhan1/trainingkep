package kookmin.uni.sugang.lecturefacility.domain.entity;

import io.naraplatform.share.domain.lang.LangStrings;
import io.naraplatform.share.domain.nara.NaraAggregate;
import io.naraplatform.share.domain.nara.NaraEntity;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Locale;

@Getter
@Setter
public class Building extends NaraEntity implements NaraAggregate {
    //
    private String buildingId;
    private String name;
    private LangStrings langNames;
    private int storyCount;
    private int lectureRoomCount;

    transient private List<Room> rooms;

    public Building(String id) {
        //
        super(id);
    }

    public Building(String buildingId,
                    String name) {
        //
        super(buildingId);
        this.name = name;
        this.langNames = new LangStrings();
    }

    public String getBuildingId() {
        //
        return getId();
    }

    public String toString() {
        //
        return toJson();
    }

    public static Building fromJson(String json) {
        //
        return JsonUtil.fromJson(json, Building.class);
    }

    public static Building sample() {
        //
        String buildingId = "B201";
        String name = "본부관";

        Building sample = new Building(
                buildingId,
                name
        );

        sample.getLangNames().addString(Locale.US.getLanguage(), "Headquarter");

        return sample;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
