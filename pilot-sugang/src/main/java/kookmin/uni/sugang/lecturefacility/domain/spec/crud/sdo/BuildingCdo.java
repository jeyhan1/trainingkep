package kookmin.uni.sugang.lecturefacility.domain.spec.crud.sdo;

import io.naraplatform.share.domain.lang.LangStrings;
import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import kookmin.uni.sugang.lecturefacility.domain.entity.Building;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BuildingCdo implements JsonSerializable {
    //
    private String buildingId;
    private String name;
    private LangStrings langNames;
    private int storyCount;
    private int lectureRoomCount;

    public BuildingCdo(String buildingId,
                    String name) {
        //
        this.buildingId = buildingId;
        this.name = name;
    }
    public String toString() {
        //
        return toJson();
    }

    public static BuildingCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, BuildingCdo.class);
    }

    public static BuildingCdo sample() {
        //
        Building building = Building.sample();
        return new BuildingCdo(
                building.getBuildingId(),
                building.getName()
        );
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}