package kookmin.uni.sugang.lecturefacility.domain.entity;

public enum RoomType {
    //
    LectureRoom,
    SeminarRoom,
    TownHall,
    MeetingRoom,
    Laboratory
}