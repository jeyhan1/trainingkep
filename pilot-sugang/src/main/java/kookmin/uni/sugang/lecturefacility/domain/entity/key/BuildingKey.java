package kookmin.uni.sugang.lecturefacility.domain.entity.key;

import io.naraplatform.share.util.json.JsonSerializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class BuildingKey implements JsonSerializable {
    // 빌딩ID, 부처ID, building key
    private String departmentId;
    private String buildingId;

    public String toString() {
        //
        return toJson();
    }


}
