package kookmin.uni.sugang.lecturefacility.domain.spec.crud.sdo;

import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import kookmin.uni.sugang.lecturefacility.domain.entity.Room;
import kookmin.uni.sugang.lecturefacility.domain.entity.RoomType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LectureRoomCdo implements JsonSerializable {
    private String buildingId;
    private String roomNo;
    private String name;
    private int capacity;
    private RoomType type;

    public String toString() {
        return toJson();
    }

    public static Room fromJson(String json) {
        return JsonUtil.fromJson(json, Room.class);
    }

    public static LectureRoomCdo sample() {
//
//        Room lectureRoom = Room.sample();
//        return new ClassRoomCdo(
//                lectureRoom.getBuildingGbn(),
//                lectureRoom.getRoomNo(),
//                lectureRoom.getRoomNm()
//        );
        return null;
    }

    public static void main(String[] args) {
        System.out.println(sample());
    }
}
