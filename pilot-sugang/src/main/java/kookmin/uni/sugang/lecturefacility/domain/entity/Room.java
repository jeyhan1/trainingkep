package kookmin.uni.sugang.lecturefacility.domain.entity;

import io.naraplatform.share.domain.NameValue;
import io.naraplatform.share.domain.NameValueList;
import io.naraplatform.share.domain.nara.NaraEntity;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Room extends NaraEntity {
    //
    private String buildingId;
    private String roomNo;
    private String name;
    private int capacity;
    private RoomType type;

    public Room(String buildingId,
                String roomNo,
                int capacity,
                String name) {
        //
        super(String.format("%s-%s", buildingId, roomNo));

        this.roomNo = roomNo;
        this.name = name;
        this.capacity = capacity;
        this.type = RoomType.LectureRoom;
        this.buildingId = buildingId;
    }

    public String toString() {
        return toJson();
    }

    public static Room fromJson(String json) {
        return JsonUtil.fromJson(json, Room.class);
    }

    public void setValues(NameValueList nameValues) {
        //
        for(NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();
            switch (nameValue.getName()) {
                case "name":
                    this.name = value;
                    break;

                default:
                    throw new IllegalArgumentException("Update not allowed: " + nameValue);
            }
        }
    }

    public static Room sample() {
        //
        String buildingId = "B010";
        String roomNo = "219";
        int seatCount = 40;
        String name = "차세대개발팀";

        Room sample = new Room(
                buildingId,
                roomNo,
                seatCount,
                name
        );

        return sample;
    }

    public static void main(String[] args) {
        System.out.println(sample());
    }
}