package kookmin.uni.sugang.lecturefacility.domain.spec.crud;

import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class OffsetLimit implements JsonSerializable {
    //
    private int offset;
    private int limit;

    public OffsetLimit() {
        //
        this.offset = 0;
        this.limit = 10;
    }

    public String toString() {
        //
        return toJson();
    }

    public static OffsetLimit fromJson(String json) {
        //
        return JsonUtil.fromJson(json, OffsetLimit.class);
    }

}
