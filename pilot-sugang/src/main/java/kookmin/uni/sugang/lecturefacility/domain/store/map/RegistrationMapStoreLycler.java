package kookmin.uni.sugang.lecturefacility.domain.store.map;

import kookmin.uni.sugang.lecturefacility.domain.store.LectureFacilityStore;
import kookmin.uni.sugang.lecturefacility.domain.store.lifecycle.RegistrationStoreLycler;

public class RegistrationMapStoreLycler implements RegistrationStoreLycler {
    //
    private static RegistrationStoreLycler singletonStoreLycler;

    private LectureFacilityStore lectureFacilityStore;

    protected RegistrationMapStoreLycler() {
        //
        this.lectureFacilityStore = new LectureFacilityMapStore();
    }

    public static RegistrationStoreLycler getInstance() {
        //
        if(singletonStoreLycler == null) {
            singletonStoreLycler = new RegistrationMapStoreLycler();
        }

        return singletonStoreLycler;
    }

    public static RegistrationStoreLycler newInstance() {
        //
        return new RegistrationMapStoreLycler();
    }

	@Override
	public LectureFacilityStore requestLectureFacilityStore() {
		return lectureFacilityStore;
	}
}
