package kookmin.uni.sugang.lecturefacility.domain.store;

import kookmin.uni.sugang.lecturefacility.domain.entity.Building;
import kookmin.uni.sugang.lecturefacility.domain.entity.Room;

import java.util.List;

public interface LectureFacilityStore {
    //
    void createBuilding(Building building);
    Building retrieveBuilding(String id);
    List<Building> retrieveByNameLike(String nameLike);
    List<Building> retrieveAll(int offset, int limit);
    void updateBuilding(Building building);
    void deleteBuilding(Building building);
    boolean exists(String buildingId);
    int count();

    void createRoom(Room room);
    Room retrieveRoom(String id);
    void updateRoom(Room room);
    void deleteRoom(Room room);
    boolean existsRoomByName(String name);
    int countRoomByBuildingId(String buildingId);
}