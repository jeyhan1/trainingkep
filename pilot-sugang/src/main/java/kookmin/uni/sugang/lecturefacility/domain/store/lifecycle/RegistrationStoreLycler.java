package kookmin.uni.sugang.lecturefacility.domain.store.lifecycle;

import kookmin.uni.sugang.lecturefacility.domain.store.LectureFacilityStore;

public interface RegistrationStoreLycler {
    //
    LectureFacilityStore requestLectureFacilityStore();
}