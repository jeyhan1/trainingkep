package kookmin.uni.sugang.lecturefacility.domain.logic;

import java.util.List;

import io.naraplatform.share.domain.NameValueList;
import kookmin.uni.sugang.lecturefacility.domain.entity.Building;
import kookmin.uni.sugang.lecturefacility.domain.entity.Room;
import kookmin.uni.sugang.lecturefacility.domain.spec.crud.LectureFacilityService;
import kookmin.uni.sugang.lecturefacility.domain.spec.crud.OffsetLimit;
import kookmin.uni.sugang.lecturefacility.domain.spec.crud.sdo.BuildingCdo;
import kookmin.uni.sugang.lecturefacility.domain.spec.crud.sdo.LectureRoomCdo;
import kookmin.uni.sugang.lecturefacility.domain.store.LectureFacilityStore;
import kookmin.uni.sugang.lecturefacility.domain.store.lifecycle.RegistrationStoreLycler;

public class LectureFacilityLogic implements LectureFacilityService {
    //
    LectureFacilityStore lectureFacilityStore;

    public LectureFacilityLogic(RegistrationStoreLycler storeLycler) {
        //
        this.lectureFacilityStore = storeLycler.requestLectureFacilityStore();
    }

    @Override
    public String registerBuilding(BuildingCdo buildingCdo) {
        return null;
    }

    @Override
    public Building findBuilding(String buildingId) {
        return null;
    }

    @Override
    public void modifyBuilding(String buildingId, NameValueList nameValues) {

    }

    @Override
    public void removeBuilding(String buildingId) {

    }

    @Override
    public String registrationRoom(LectureRoomCdo lectureRoomCdo) {
        return null;
    }

    @Override
    public Room findRoom(String classRoomId) {
        return null;
    }

    @Override
    public List<Room> findRoomByBuildingId(String buidlingId, OffsetLimit offsetLimit) {
        return null;
    }

    @Override
    public void modifyRoom(String classRoomId, NameValueList nameValueList) {

    }

    @Override
    public void removeRoom(String classRoomId) {

    }
    //
//    private BuildingStore buildingStore;
//
//    public BuildingLogic(RegistrationStoreLycler storeLycler) {
//        //
//        this.buildingStore = storeLycler.requestClassRoomStore();
//    }
//
//    @Override
//    public String registrationClassRoom(ClassRoomCdo classRoomCdo) {
//        //
//        String roomNm = classRoomCdo.getRoomNm();
//        if(buildingStore.existsByRoomName(roomNm)) {
//            throw new AlreadyExistsException("Room Name: " + roomNm);
//        }
//
//        Room lectureRoom = new Room(
//                classRoomCdo.getBuildingGbn(),
//                classRoomCdo.getRoomNo(),
//                roomNm
//        );
//
//        buildingStore.create(lectureRoom);
//
//        return lectureRoom.getId();
//    }
//
//    @Override
//    public Room findClassRoom(String classRoomId) {
//        //
//        Room lectureRoom = buildingStore.retrieve(classRoomId);
//        if(lectureRoom == null) {
//            throw new NoSuchElementException("Room id: " + classRoomId);
//        }
//
//        return lectureRoom;
//    }
//
//    @Override
//    public void modifyClassRoom(String classRoomId, NameValueList nameNames) {
//        //
//        Room lectureRoom = findClassRoom(classRoomId);
//        lectureRoom.setValues(nameNames);
//
//        buildingStore.update(lectureRoom);
//    }
//
//    @Override
//    public void removeClassRoom(String classRoomId) {
//        //
//        Room lectureRoom = findClassRoom(classRoomId);
//        //
//        // do something
//
//        buildingStore.delete(lectureRoom);
//    }
}