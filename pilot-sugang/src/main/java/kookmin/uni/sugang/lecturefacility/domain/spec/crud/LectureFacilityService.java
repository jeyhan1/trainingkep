package kookmin.uni.sugang.lecturefacility.domain.spec.crud;

import io.naraplatform.share.domain.NameValueList;
import kookmin.uni.sugang.lecturefacility.domain.entity.Building;
import kookmin.uni.sugang.lecturefacility.domain.entity.Room;
import kookmin.uni.sugang.lecturefacility.domain.spec.crud.sdo.BuildingCdo;
import kookmin.uni.sugang.lecturefacility.domain.spec.crud.sdo.LectureRoomCdo;

import java.util.List;

public interface LectureFacilityService {
    //
    String registerBuilding(BuildingCdo buildingCdo);
    Building findBuilding(String buildingId);
    void modifyBuilding(String buildingId, NameValueList nameValues);
    void removeBuilding(String buildingId);

    String registrationRoom(LectureRoomCdo lectureRoomCdo);
    Room findRoom(String lectureRoomId);
    List<Room> findRoomByBuildingId(String buidlingId, OffsetLimit offsetLimit);
    void modifyRoom(String lectureRoomId, NameValueList nameValueList);
    void removeRoom(String lectureRoomId);
}