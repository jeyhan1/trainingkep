package kookmin.uni.sugang.lecturefacility.domain.spec.lifecycle.pojo;

import kookmin.uni.sugang.lecturefacility.domain.spec.crud.LectureFacilityService;
import kookmin.uni.sugang.lecturefacility.domain.spec.lifecycle.RegistrationServiceLycler;
import kookmin.uni.sugang.lecturefacility.domain.store.lifecycle.RegistrationStoreLycler;
import kookmin.uni.sugang.lecturefacility.domain.store.map.RegistrationMapStoreLycler;

public class RegistrationPojoServiceLycler implements RegistrationServiceLycler {
    //
    private static RegistrationServiceLycler singletonServiceLycler;

    private LectureFacilityService lectureFacilityService;

    public RegistrationPojoServiceLycler() {
        //
        RegistrationStoreLycler storeLycler = RegistrationMapStoreLycler.getInstance();
    }

    public static RegistrationServiceLycler getInstance() {
        //
        if(singletonServiceLycler == null) {
            singletonServiceLycler = new RegistrationPojoServiceLycler();
        }

        return singletonServiceLycler;
    }

    public static RegistrationServiceLycler newInstance() {
        //
        return new RegistrationPojoServiceLycler();
    }

    @Override
    public LectureFacilityService requestLectureFacilityService() {
        return lectureFacilityService;
    }

 }
