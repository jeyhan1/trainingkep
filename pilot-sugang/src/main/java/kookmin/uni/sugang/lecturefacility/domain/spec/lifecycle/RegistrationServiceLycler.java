package kookmin.uni.sugang.lecturefacility.domain.spec.lifecycle;

import kookmin.uni.sugang.lecturefacility.domain.spec.crud.LectureFacilityService;

public interface RegistrationServiceLycler {
    //
    LectureFacilityService requestLectureFacilityService();
}
