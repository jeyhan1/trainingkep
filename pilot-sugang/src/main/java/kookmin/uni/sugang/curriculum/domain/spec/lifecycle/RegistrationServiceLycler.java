package kookmin.uni.sugang.curriculum.domain.spec.lifecycle;

import kookmin.uni.sugang.curriculum.domain.spec.crud.CurriculumService;

public interface RegistrationServiceLycler {
    //
    CurriculumService requestCurriculumService();
}
