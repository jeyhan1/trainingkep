package kookmin.uni.sugang.curriculum.domain.spec.crud.sdo;

import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import kookmin.uni.sugang.curriculum.domain.entity.Course;
import kookmin.uni.sugang.curriculum.domain.entity.SemesterType;
import kookmin.uni.sugang.curriculum.domain.entity.SubjectType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CourseCdo implements JsonSerializable {
    //
    private String schoolYear;
    private SemesterType semesterType;
    private String departmentId;
    private String majorId;
    private String subjectId;
    private String grade;
    private String credit;
    private SubjectType subjectType;

    public String toString() {
        return toJson();
    }

    public static CourseCdo fromJson(String json) {
        return JsonUtil.fromJson(json, CourseCdo.class);
    }

    public static CourseCdo sample() {

        Course course = Course.sample();
        return new CourseCdo(
                course.getSchoolYear(),
                course.getSemesterType(),
                course.getDepartmentId(),
                course.getMajorId(),
                course.getSubjectId(),
                course.getGrade(),
                course.getCredit(),
                course.getSubjectType()
        );
    }

    public static void main(String[] args) {
        System.out.println(sample());
    }
}
