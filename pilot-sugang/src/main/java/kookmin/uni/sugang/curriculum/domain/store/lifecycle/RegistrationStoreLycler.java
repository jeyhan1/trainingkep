package kookmin.uni.sugang.curriculum.domain.store.lifecycle;

import kookmin.uni.sugang.curriculum.domain.store.CurriculumStore;

public interface RegistrationStoreLycler {
    //
    CurriculumStore requestCurriculumStore();
}