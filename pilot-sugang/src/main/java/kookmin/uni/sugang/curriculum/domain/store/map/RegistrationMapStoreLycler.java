package kookmin.uni.sugang.curriculum.domain.store.map;

import kookmin.uni.sugang.curriculum.domain.store.CurriculumStore;
import kookmin.uni.sugang.curriculum.domain.store.lifecycle.RegistrationStoreLycler;

public class RegistrationMapStoreLycler implements RegistrationStoreLycler {
    //
    private static RegistrationStoreLycler singletonStoreLycler;

    private CurriculumStore curriculumStore;

    protected RegistrationMapStoreLycler() {
        //
        this.curriculumStore = new CurriculumMapStore();
    }

    public static RegistrationStoreLycler getInstance() {
        //
        if(singletonStoreLycler == null) {
            singletonStoreLycler = new RegistrationMapStoreLycler();
        }

        return singletonStoreLycler;
    }

    public static RegistrationStoreLycler newInstance() {
        //
        return new RegistrationMapStoreLycler();
    }

    @Override
    public CurriculumStore requestCurriculumStore() {
        //
        return this.curriculumStore;
    }
}
