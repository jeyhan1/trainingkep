package kookmin.uni.sugang.curriculum.domain.store;

import kookmin.uni.sugang.curriculum.domain.entity.Course;
import kookmin.uni.sugang.curriculum.domain.entity.Subject;

public interface CurriculumStore {
    //
    void createSubject(Subject subject);
    Subject retrieveSubject(String id);
    void updateSubject(Subject id);
    void deleteSubject(Subject id);
    boolean existsBySubjectName(String name);

    void createCourse(Course course);
    Course retrieveCourse(String id);
    void upateCourse(Course course);
    void deleteCourse(Course course);
}