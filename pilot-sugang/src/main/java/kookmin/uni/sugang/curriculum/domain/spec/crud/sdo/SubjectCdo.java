package kookmin.uni.sugang.curriculum.domain.spec.crud.sdo;

import io.naraplatform.share.domain.lang.LangStrings;
import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import kookmin.uni.sugang.curriculum.domain.entity.Subject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SubjectCdo implements JsonSerializable {
    //
    private String subjectId;
    private String name;
    private int credit;
    private LangStrings langNames;

    public SubjectCdo(String subjectId,
                   String name,
                   int credit) {
        //
        super();
        this.subjectId = subjectId;
        this.name = name;
        this.credit = credit;
    }

    public String toString() {
        //
        return toJson();
    }

    public static SubjectCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, SubjectCdo.class);
    }

    public static SubjectCdo sample() {
        //
        Subject subject = Subject.sample();
        return new SubjectCdo(
                subject.getSubjectId(),
                subject.getName(),
                subject.getCredit()
        );
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
