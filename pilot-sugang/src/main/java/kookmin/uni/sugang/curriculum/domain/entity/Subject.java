package kookmin.uni.sugang.curriculum.domain.entity;

import io.naraplatform.share.domain.NameValue;
import io.naraplatform.share.domain.NameValueList;
import io.naraplatform.share.domain.lang.LangStrings;
import io.naraplatform.share.domain.nara.NaraAggregate;
import io.naraplatform.share.domain.nara.NaraEntity;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class Subject extends NaraEntity{
    //
    private String subjectId;
    private String name;
    private int credit;
    private LangStrings langNames;

    public Subject(String id) {
        //
        super(id);
    }

    public Subject(String subjectId,
                   String name,
                   int credit) {
        //
        super(subjectId);
        this.name = name;
        this.credit = credit;
    }

    public String getSubjectId() {
        //
        return getId();
    }

    public String toString() {
        //
        return toJson();
    }

    public static Subject fromJson(String json) {
        //
        return JsonUtil.fromJson(json, Subject.class);
    }

    public void setValues(NameValueList nameValues) {
        //
        for(NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();
            switch (nameValue.getName()) {
                case "name":
                    this.name = value;
                    break;
                case "credit":
                    this.credit = Integer.valueOf(value);
                    break;
                default:
                    throw new IllegalArgumentException("Update not allowed: " + nameValue);
            }
        }
    }

    public static Subject sample() {
        //
        String subjectId = "S10";
        String name = "공학수학";
        int credit = 3;

        Subject sample = new Subject(
                subjectId,
                name,
                credit
        );

        return sample;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}