package kookmin.uni.sugang.curriculum.domain.logic;

import java.util.NoSuchElementException;

import io.naraplatform.share.domain.NameValueList;
import io.naraplatform.share.exception.store.AlreadyExistsException;
import kookmin.uni.sugang.curriculum.domain.entity.Course;
import kookmin.uni.sugang.curriculum.domain.entity.Subject;
import kookmin.uni.sugang.curriculum.domain.spec.crud.CurriculumService;
import kookmin.uni.sugang.curriculum.domain.spec.crud.sdo.CourseCdo;
import kookmin.uni.sugang.curriculum.domain.spec.crud.sdo.SubjectCdo;
import kookmin.uni.sugang.curriculum.domain.store.CurriculumStore;
import kookmin.uni.sugang.curriculum.domain.store.lifecycle.RegistrationStoreLycler;

public class CurriculumLogic implements CurriculumService {
    //
    private CurriculumStore curriculumStore;

    public CurriculumLogic(RegistrationStoreLycler storeLycler) {
        //
        this.curriculumStore = storeLycler.requestCurriculumStore();
    }

    @Override
    public String registerSubject(SubjectCdo subjectCdo) {
        //
        String name = subjectCdo.getName();
        if(curriculumStore.existsBySubjectName(name)) {
            throw new AlreadyExistsException("Subject Name: " + name);
        }

        Subject subject = new Subject(
                subjectCdo.getSubjectId(),
                name,
                subjectCdo.getCredit()
        );

        curriculumStore.createSubject(subject);

        return subject.getId();
    }

    @Override
    public Subject findSubject(String subjectCd) {
        //
        Subject subject = curriculumStore.retrieveSubject(subjectCd);
        if(subject == null) {
            throw new NoSuchElementException("Subject Code: " + subjectCd);
        }

        return subject;
    }

    @Override
    public void modifySubject(String subjectCd, NameValueList nameNames) {
        //
        Subject subject = findSubject(subjectCd);
        subject.setValues(nameNames);

        curriculumStore.updateSubject(subject);
    }

    @Override
    public void removeSubject(String subjectCd) {
        //
        Subject subject = findSubject(subjectCd);
        //
        // do something

        curriculumStore.deleteSubject(subject);
    }

    @Override
    public String registrationCourse(CourseCdo courseCdo) {
        //
        String subjectCd = courseCdo.getSubjectId();

        Course course = new Course(
                courseCdo.getSchoolYear(),
                courseCdo.getSemesterType(),
                courseCdo.getDepartmentId(),
                courseCdo.getMajorId(),
                courseCdo.getSubjectId(),
                courseCdo.getGrade(),
                courseCdo.getCredit(),
                courseCdo.getSubjectType()
        );

        curriculumStore.createCourse(course);

        return course.getId();
    }

    @Override
    public Course findCourse(String courseId) {
        Course course = curriculumStore.retrieveCourse(courseId);
        if(course == null) {
            throw new NoSuchElementException("Course id: " + courseId);
        }
        return course;
    }

    @Override
    public void modifyCourse(String courseId, NameValueList nameValueList) {
        Course course = findCourse(courseId);
        course.setValues(nameValueList);

        curriculumStore.upateCourse(course);
    }

    @Override
    public void removeCourse(String courseId) {
        Course course = findCourse(courseId);
        curriculumStore.deleteCourse(course);
    }
}