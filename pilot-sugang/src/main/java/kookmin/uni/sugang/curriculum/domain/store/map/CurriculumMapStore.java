package kookmin.uni.sugang.curriculum.domain.store.map;

import java.util.LinkedHashMap;
import java.util.Map;

import kookmin.uni.sugang.curriculum.domain.entity.Course;
import kookmin.uni.sugang.curriculum.domain.entity.Subject;
import kookmin.uni.sugang.curriculum.domain.store.CurriculumStore;

public class CurriculumMapStore implements CurriculumStore {
    //
    private Map<String, Subject> subjectMap;
    private Map<String, Course> courseOpenMap;

    public CurriculumMapStore() {
        //
        this.subjectMap = new LinkedHashMap<>();
        this.courseOpenMap = new LinkedHashMap<>();
    }

    @Override
    public void createSubject(Subject subject) {
        //
        subjectMap.put(subject.getId(), subject);
    }

    @Override
    public Subject retrieveSubject(String id) {
        //
        return subjectMap.get(id);
    }

    @Override
    public void updateSubject(Subject subject) {
        //
        // do nothing
    }

    @Override
    public void deleteSubject(Subject subject) {
        //
        subjectMap.remove(subject.getId());
    }

    @Override
    public boolean existsBySubjectName(String name) {
        //
        for(Subject subject : subjectMap.values()) {
            if(subject.getName().equals(name)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void createCourse(Course course) {
        courseOpenMap.put(course.getId(), course);
    }

    @Override
    public Course retrieveCourse(String id) {
        return courseOpenMap.get(id);
    }

    @Override
    public void upateCourse(Course course) {
        // do noting
    }

    @Override
    public void deleteCourse(Course course) {
        courseOpenMap.remove(course.getId());
    }

}
