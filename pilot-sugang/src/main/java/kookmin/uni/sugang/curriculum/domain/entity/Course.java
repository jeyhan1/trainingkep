package kookmin.uni.sugang.curriculum.domain.entity;

import io.naraplatform.share.domain.NameValue;
import io.naraplatform.share.domain.NameValueList;
import io.naraplatform.share.domain.nara.NaraEntity;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Course extends NaraEntity {
    //
    private String schoolYear;
    private SemesterType semesterType;
    private String departmentId;
    private String majorId;
    private String subjectId;
    private String grade;
    private String credit;
    private SubjectType subjectType;

    public Course(String schoolYear,
                  SemesterType semesterType,
                  String departmentId,
                  String majorId,
                  String subjectId,
                  String grade,
                  String credit,
                  SubjectType subjectType
                  ) {
        //
        super();
        this.schoolYear = schoolYear;
        this.semesterType = semesterType;
        this.departmentId = departmentId;
        this.majorId = majorId;
        this.subjectId = subjectId;
        this.grade = grade;
        this.credit = credit;
        this.subjectType = subjectType;
    }

    public String toString() {
        return toJson();
    }

    public static Course fromJson(String json) {
        return JsonUtil.fromJson(json, Course.class);
    }

    public void setValues(NameValueList nameValues) {
        //
        for(NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();
            switch (nameValue.getName()) {
                case "grade":
                    this.credit = value;
                    break;
                case "credit":
                    this.credit = value;
                    break;
                default:
                    throw new IllegalArgumentException("Update not allowed: " + nameValue);
            }
        }
    }

    public static Course sample() {
        //
        String year = "2019";
        String departmentId = "computer";
        String majorId = "CS";
        String subjectid = "MSA";
        String grade = "4";
        String credit = "3";

        Course sample = new Course(year,
                SemesterType.First,
                departmentId,
                majorId,
                subjectid,
                grade,
                credit,
                SubjectType.MajorNecessary);

        return sample;
    }

    public static void main(String[] args) {
        System.out.println(sample());
    }

}
