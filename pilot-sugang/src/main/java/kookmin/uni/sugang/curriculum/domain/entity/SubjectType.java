package kookmin.uni.sugang.curriculum.domain.entity;

public enum SubjectType {
    //
    MajorNecessary("1"),
    MajorSelection("2"),
    CultureNecessary("3"),
    CultureSelection("4");

    private String code;

    SubjectType(String code) {
        //
        this.code = code;
    }

    public String code() {
        //
        return code;
    }

    public SubjectType fromCode(String code) {
        //
        return null;
    }

    public boolean equalsFromCode(String code) {
        //
        return false;
    }

}
