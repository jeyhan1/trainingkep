package kookmin.uni.sugang.curriculum.domain.spec.lifecycle.pojo;

import kookmin.uni.sugang.curriculum.domain.logic.CurriculumLogic;
import kookmin.uni.sugang.curriculum.domain.spec.crud.CurriculumService;
import kookmin.uni.sugang.curriculum.domain.spec.lifecycle.RegistrationServiceLycler;
import kookmin.uni.sugang.curriculum.domain.store.lifecycle.RegistrationStoreLycler;
import kookmin.uni.sugang.curriculum.domain.store.map.RegistrationMapStoreLycler;

public class RegistrationPojoServiceLycler implements RegistrationServiceLycler {
    //
    private static RegistrationServiceLycler singletonServiceLycler;

    private CurriculumService curriculumService;

    public RegistrationPojoServiceLycler() {
        //
        RegistrationStoreLycler storeLycler = RegistrationMapStoreLycler.getInstance();
        this.curriculumService = new CurriculumLogic(storeLycler);
    }

    public static RegistrationServiceLycler getInstance() {
        //
        if(singletonServiceLycler == null) {
            singletonServiceLycler = new RegistrationPojoServiceLycler();
        }

        return singletonServiceLycler;
    }

    public static RegistrationServiceLycler newInstance() {
        //
        return new RegistrationPojoServiceLycler();
    }

    @Override
    public CurriculumService requestCurriculumService() {
        //
        return curriculumService;
    }

 }
