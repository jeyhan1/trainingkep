package kookmin.uni.sugang.curriculum.domain.spec.crud;

import io.naraplatform.share.domain.NameValueList;
import kookmin.uni.sugang.curriculum.domain.entity.Course;
import kookmin.uni.sugang.curriculum.domain.entity.Subject;
import kookmin.uni.sugang.curriculum.domain.spec.crud.sdo.CourseCdo;
import kookmin.uni.sugang.curriculum.domain.spec.crud.sdo.SubjectCdo;

public interface CurriculumService {
    //
    String registerSubject(SubjectCdo subjectCdo);
    Subject findSubject(String subjectId);
    void modifySubject(String subjectId, NameValueList nameNames);
    void removeSubject(String subjectId);

    String registrationCourse(CourseCdo CourseCdo);
    Course findCourse(String courseId);
    void modifyCourse(String courseId, NameValueList nameValueList);
    void removeCourse(String courseId);
}