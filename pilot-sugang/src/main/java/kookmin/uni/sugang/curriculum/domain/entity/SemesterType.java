package kookmin.uni.sugang.curriculum.domain.entity;

public enum SemesterType {
    //
    First("1"),
    SummerSeason("2"),
    Second("3"),
    WinterSeason("4");

    private String code;

    SemesterType(String code) {
        //
        this.code = code;
    }

    public String code() {
        //
        return code;
    }

    public SemesterType fromCode(String code) {
        //
        return null;
    }

    public boolean equalsFromCode(String code) {
        //
        return false;
    }
}