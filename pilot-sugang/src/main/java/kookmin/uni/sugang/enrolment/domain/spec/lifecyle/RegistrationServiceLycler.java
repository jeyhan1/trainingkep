package kookmin.uni.sugang.enrolment.domain.spec.lifecyle;

import kookmin.uni.sugang.enrolment.domain.spec.crud.EnrolmentService;

public interface RegistrationServiceLycler {
    //
    EnrolmentService requestEnrolmentService();
}
