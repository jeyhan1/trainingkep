package kookmin.uni.sugang.enrolment.domain.entity;

import io.naraplatform.share.domain.nara.NaraEntity;
import io.naraplatform.share.util.json.JsonUtil;
import kookmin.uni.sugang.curriculum.domain.entity.SemesterType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LectureOffering extends NaraEntity {
    //
    private String schoolYear;
    private SemesterType semesterType;
    private String departmentId;
    private String majorId;
    private String subjectId;
    private String classNo;
    private String studentNo;
    private ResultType resultType;

    public LectureOffering(String id) {
        //
        super(id);
    }

    public LectureOffering(String schoolYear,
                           SemesterType semesterType,
                           String departmentId,
                           String majorId,
                           String subjectId,
                           String classNo,
                           String studentNo
                    ) {
        //
        super();
        this.schoolYear = schoolYear;
        this.semesterType = semesterType;
        this.departmentId = departmentId;
        this.majorId = majorId;
        this.subjectId = subjectId;
        this.classNo = classNo;
        this.studentNo = studentNo;
    }

    public String toString() {
        //
        return toJson();
    }

    public static LectureOffering fromJson(String json) {
        //
        return JsonUtil.fromJson(json, LectureOffering.class);
    }

    public static LectureOffering sample() {
        //
        String schoolYear   = "2019";
        String departmentId = "A001";
        String majorId      = "M001";
        String subjectId    = "S001";
        String classNo      = "M102221";
        String studentNo    = "9006049";

        LectureOffering sample = new LectureOffering(
                schoolYear
                , SemesterType.First
                ,departmentId
                ,majorId
                ,subjectId
                ,classNo
                ,studentNo
                ,ResultType.Success
        );

        return sample;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}