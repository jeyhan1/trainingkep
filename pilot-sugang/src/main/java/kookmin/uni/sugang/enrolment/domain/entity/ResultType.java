package kookmin.uni.sugang.enrolment.domain.entity;

public enum ResultType {
    Success("1"),
    Failure("2");

    private String code;

    ResultType(String code) {
        //
        this.code = code;
    }

    public String code() {
        //
        return code;
    }

    public ResultType fromCode(String code) {
        //
        return null;
    }

    public boolean equalsFromCode(String code) {
        //
        return false;
    }
}
