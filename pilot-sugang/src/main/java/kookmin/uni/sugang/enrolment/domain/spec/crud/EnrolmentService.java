package kookmin.uni.sugang.enrolment.domain.spec.crud;

import io.naraplatform.share.domain.NameValueList;
import kookmin.uni.sugang.enrolment.domain.entity.LectureOffering;
import kookmin.uni.sugang.enrolment.domain.spec.crud.sdo.LectureOfferingCdo;


public interface EnrolmentService {
    //
    String registerLectureOffering(LectureOfferingCdo lectureOfferingCdo);
    LectureOffering findLectureOffering(String lectureId);
    void modifyLectureOffering(String lectureId, NameValueList nameNames);
    void removeLectureOffering(String lectureId);
}