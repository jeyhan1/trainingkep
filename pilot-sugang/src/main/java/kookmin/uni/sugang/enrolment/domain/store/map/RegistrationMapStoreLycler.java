package kookmin.uni.sugang.enrolment.domain.store.map;

import kookmin.uni.sugang.enrolment.domain.store.EnrolmentStore;
import kookmin.uni.sugang.enrolment.domain.store.lifecycle.RegistrationStoreLycler;

public class RegistrationMapStoreLycler  implements RegistrationStoreLycler {
    //
    private static RegistrationStoreLycler singletonStoreLycler;

    private EnrolmentStore enrolmentStore;

    protected RegistrationMapStoreLycler() {
        //
        this.enrolmentStore = new EnrolmentMapStore();
    }

    public static RegistrationStoreLycler getInstance() {
        //
        if(singletonStoreLycler == null) {
            singletonStoreLycler = new RegistrationMapStoreLycler();
        }

        return singletonStoreLycler;
    }

    public static RegistrationStoreLycler newInstance() {
        //
        return new RegistrationMapStoreLycler();
    }

    @Override
    public EnrolmentStore requestEnrolmentStore() {
        return enrolmentStore;
    }
}
