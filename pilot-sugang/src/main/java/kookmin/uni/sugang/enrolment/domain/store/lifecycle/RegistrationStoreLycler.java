package kookmin.uni.sugang.enrolment.domain.store.lifecycle;

import kookmin.uni.sugang.enrolment.domain.store.EnrolmentStore;

public interface RegistrationStoreLycler {
    //
    EnrolmentStore requestEnrolmentStore();
}