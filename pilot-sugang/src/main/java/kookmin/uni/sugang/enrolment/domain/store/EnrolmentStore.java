package kookmin.uni.sugang.enrolment.domain.store;

import kookmin.uni.sugang.enrolment.domain.entity.LectureOffering;

public interface EnrolmentStore {
    //
    void createLecture(LectureOffering lectureOffering);
    LectureOffering retrieveLecture(String id);
    void updateLecture(LectureOffering lectureOffering);
    void deleteLecture(LectureOffering lectureOffering);
    boolean existsBySubjectId(String subjectId);
}