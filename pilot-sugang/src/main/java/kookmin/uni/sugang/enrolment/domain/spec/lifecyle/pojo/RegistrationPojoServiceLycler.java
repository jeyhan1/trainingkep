package kookmin.uni.sugang.enrolment.domain.spec.lifecyle.pojo;

import kookmin.uni.sugang.enrolment.domain.logic.EnrolmentLogic;
import kookmin.uni.sugang.enrolment.domain.spec.crud.EnrolmentService;
import kookmin.uni.sugang.enrolment.domain.spec.lifecyle.RegistrationServiceLycler;
import kookmin.uni.sugang.enrolment.domain.store.lifecycle.RegistrationStoreLycler;
import kookmin.uni.sugang.enrolment.domain.store.map.RegistrationMapStoreLycler;

public class RegistrationPojoServiceLycler implements RegistrationServiceLycler {
    //
    private static RegistrationServiceLycler singletonServiceLycler;

    private EnrolmentService enrolmentService;

    public RegistrationPojoServiceLycler() {
        //
        RegistrationStoreLycler storeLycler = RegistrationMapStoreLycler.getInstance();
        this.enrolmentService = new EnrolmentLogic(storeLycler);
    }

    public static RegistrationServiceLycler getInstance() {
        //
        if(singletonServiceLycler == null) {
            singletonServiceLycler = new RegistrationPojoServiceLycler();
        }

        return singletonServiceLycler;
    }

    public static RegistrationServiceLycler newInstance() {
        //
        return new RegistrationPojoServiceLycler();
    }

    @Override
    public EnrolmentService requestEnrolmentService() {
        //
        return enrolmentService;
    }

 }
