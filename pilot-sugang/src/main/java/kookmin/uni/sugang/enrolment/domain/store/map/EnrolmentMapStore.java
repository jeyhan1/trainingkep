package kookmin.uni.sugang.enrolment.domain.store.map;

import java.util.LinkedHashMap;
import java.util.Map;

import kookmin.uni.sugang.enrolment.domain.entity.LectureOffering;
import kookmin.uni.sugang.enrolment.domain.store.EnrolmentStore;

public class EnrolmentMapStore implements EnrolmentStore {
    //
    private Map<String, LectureOffering> lectureMap;

    public EnrolmentMapStore() {
        //
        this.lectureMap = new LinkedHashMap<>();
    }

    @Override
    public void createLecture(LectureOffering lectureOffering) {
        //
        lectureMap.put(lectureOffering.getId(), lectureOffering);
    }

    @Override
    public LectureOffering retrieveLecture(String id) {
        //
        return lectureMap.get(id);
    }

    @Override
    public void updateLecture(LectureOffering lectureOffering) {
        //
        // do nothing
    }

    @Override
    public void deleteLecture(LectureOffering lectureOffering) {
        //
        lectureMap.remove(lectureOffering.getId());
    }

    @Override
    public boolean existsBySubjectId(String subjectId) {
        for (LectureOffering lectureOffering : lectureMap.values()) {
            if (lectureOffering.getSubjectId().equals(subjectId)) {
                return true;
            }
        }
        return false;
    }
}
