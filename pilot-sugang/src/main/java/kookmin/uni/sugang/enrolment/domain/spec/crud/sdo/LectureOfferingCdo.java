package kookmin.uni.sugang.enrolment.domain.spec.crud.sdo;

import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import kookmin.uni.sugang.curriculum.domain.entity.SemesterType;
import kookmin.uni.sugang.enrolment.domain.entity.ResultType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LectureOfferingCdo implements JsonSerializable {
    //
    private String schoolYear;
    private SemesterType semesterType;
    private String departmentId;
    private String majorId;
    private String subjectId;
    private String classNo;
    private String studentNo;
    private ResultType resultType;

    public String toString() {
        //
        return toJson();
    }

    public static LectureOfferingCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, LectureOfferingCdo.class);
    }

    public static LectureOfferingCdo sample() {
        //
        String schoolYear   = "2019";
        String departmentId = "A001";
        String majorId      = "M001";
        String subjectId    = "S001";
        String classNo      = "M102221";
        String studentNo    = "9006049";
        ResultType resultType = ResultType.Success;

        LectureOfferingCdo sample = new LectureOfferingCdo(
                schoolYear
                ,SemesterType.First
                ,departmentId
                ,majorId
                ,subjectId
                ,classNo
                ,studentNo
                ,resultType
        );

        return sample;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
