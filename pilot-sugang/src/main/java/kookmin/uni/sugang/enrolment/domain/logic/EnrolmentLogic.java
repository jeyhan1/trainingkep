package kookmin.uni.sugang.enrolment.domain.logic;

import io.naraplatform.share.domain.NameValueList;
import io.naraplatform.share.exception.store.AlreadyExistsException;
import kookmin.uni.sugang.enrolment.domain.entity.LectureOffering;
import kookmin.uni.sugang.enrolment.domain.spec.crud.EnrolmentService;
import kookmin.uni.sugang.enrolment.domain.spec.crud.sdo.LectureOfferingCdo;
import kookmin.uni.sugang.enrolment.domain.store.EnrolmentStore;
import kookmin.uni.sugang.enrolment.domain.store.lifecycle.RegistrationStoreLycler;

import java.util.NoSuchElementException;

public class EnrolmentLogic implements EnrolmentService {
    //
    private EnrolmentStore enrolmentStore;


    public EnrolmentLogic(RegistrationStoreLycler storeLycler) {
        //
        this.enrolmentStore = storeLycler.requestEnrolmentStore();
    }

    @Override
    public String registerLectureOffering(LectureOfferingCdo lectureOfferingCdo) {
        //
        String sid = lectureOfferingCdo.getSubjectId();
        if (enrolmentStore.existsBySubjectId(sid)) {
            throw new AlreadyExistsException("Sid: " + sid);
        }

        LectureOffering lectureOffering = new LectureOffering(
                lectureOfferingCdo.getSchoolYear(),
                lectureOfferingCdo.getSemesterType(),
                lectureOfferingCdo.getDepartmentId(),
                lectureOfferingCdo.getMajorId(),
                lectureOfferingCdo.getSubjectId(),
                lectureOfferingCdo.getClassNo(),
                lectureOfferingCdo.getStudentNo()
        );

        enrolmentStore.createLecture(lectureOffering);

        return lectureOffering.getId();
    }

    @Override
    public LectureOffering findLectureOffering(String lectureId) {
        //
        LectureOffering lectureOffering = enrolmentStore.retrieveLecture(lectureId);
        if (lectureOffering == null) {
            throw new NoSuchElementException("LectureOffering id: " + lectureId);
        }

        return lectureOffering;
    }

    @Override
    public void modifyLectureOffering(String lectureId, NameValueList nameNames) {
        //
        LectureOffering lectureOffering = findLectureOffering(lectureId);

        enrolmentStore.updateLecture(lectureOffering);
    }

    @Override
    public void removeLectureOffering(String lectureId) {
        //
        LectureOffering lectureOffering = findLectureOffering(lectureId);
        //
        // do something

        enrolmentStore.deleteLecture(lectureOffering);
    }
}