package kookmin.uni.sugang.lectureopen.domain.spec.crud.sdo;

import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import kookmin.uni.sugang.curriculum.domain.entity.SemesterType;
import kookmin.uni.sugang.curriculum.domain.entity.SubjectType;
import kookmin.uni.sugang.lectureopen.domain.entity.DayType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LectureOpeningCdo implements JsonSerializable {
    //
    private String schoolYear;
    private SemesterType semesterType;
    private String departmentId;
    private String majorId;
    private String subjectId;
    private String classNo;
    private SubjectType subjectType;
    private String professorNo;
    private int credit;
    private String buildingId;
    private String roomNo;
    private String limitNumber;
    private DayType dayType;
    private String startTime;

    public String toString() {
        //
        return toJson();
    }

    public static LectureOpeningCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, LectureOpeningCdo.class);
    }

    public static LectureOpeningCdo sample() {
        //
        String schoolYear   = "2019";
        String departmentId = "A001";
        String majorId      = "M001";
        String subjectId    = "S001";
        String classNo      = "M102221";
        String professorNo  = "152468";
        int   credit       = 3;
        String buildingId = "DDD";
        String roomNo = "219";
        String limitNumber  = "30";
        DayType dayType = DayType.Friday;
        String startTime = "10:00";

        LectureOpeningCdo sample = new LectureOpeningCdo(
                schoolYear
                ,SemesterType.First
                ,departmentId
                ,majorId
                ,subjectId
                ,classNo
                ,SubjectType.MajorNecessary
                ,professorNo
                ,credit
                ,buildingId
                ,roomNo
                ,limitNumber
                ,dayType
                ,startTime
        );

        return sample;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
