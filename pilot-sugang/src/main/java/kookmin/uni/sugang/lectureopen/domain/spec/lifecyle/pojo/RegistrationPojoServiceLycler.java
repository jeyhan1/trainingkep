package kookmin.uni.sugang.lectureopen.domain.spec.lifecyle.pojo;

import kookmin.uni.sugang.lectureopen.domain.store.lifecycle.RegistrationStoreLycler;
import kookmin.uni.sugang.lectureopen.domain.logic.LectureOpenLogic;
import kookmin.uni.sugang.lectureopen.domain.spec.crud.LectureOpenService;
import kookmin.uni.sugang.lectureopen.domain.spec.lifecyle.RegistrationServiceLycler;
import kookmin.uni.sugang.lectureopen.domain.store.map.RegistrationMapStoreLycler;

public class RegistrationPojoServiceLycler implements RegistrationServiceLycler {
    //
    private static RegistrationServiceLycler singletonServiceLycler;

    private LectureOpenService lectureOpenService;

    public RegistrationPojoServiceLycler() {
        //
        RegistrationStoreLycler storeLycler = RegistrationMapStoreLycler.getInstance();
        this.lectureOpenService = new LectureOpenLogic(storeLycler);
    }

    public static RegistrationServiceLycler getInstance() {
        //
        if(singletonServiceLycler == null) {
            singletonServiceLycler = new RegistrationPojoServiceLycler();
        }

        return singletonServiceLycler;
    }

    public static RegistrationServiceLycler newInstance() {
        //
        return new RegistrationPojoServiceLycler();
    }

    @Override
    public LectureOpenService requestLectureOpenService() {
        //
        return lectureOpenService;
    }

 }
