package kookmin.uni.sugang.lectureopen.domain.entity;

import io.naraplatform.share.domain.NameValue;
import io.naraplatform.share.domain.NameValueList;
import io.naraplatform.share.domain.nara.NaraEntity;
import io.naraplatform.share.util.json.JsonUtil;
import kookmin.uni.sugang.curriculum.domain.entity.SemesterType;
import kookmin.uni.sugang.curriculum.domain.entity.SubjectType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LectureOpening extends NaraEntity {
    //
    private String schoolYear;
    private SemesterType semesterType;
    private String departmentId;
    private String majorId;
    private String subjectId;
    private String classNo;
    private SubjectType subjectType;
    private String professorNo;
    private int credit;
    private String buildingId;
    private String roomNo;
    private String limitNumber;
    private DayType dayType;
    private String startTime;

    public LectureOpening(String id) {
        //
        super(id);
    }

    public LectureOpening(String schoolYear,
                          SemesterType semesterType,
                          String departmentId,
                          String majorId,
                          String subjectId,
                          String classNo,
                          SubjectType subjectType,
                          String professorNo,
                          int credit
                    ) {
        //
        super();
        this.schoolYear = schoolYear;
        this.semesterType = semesterType;
        this.departmentId = departmentId;
        this.majorId = majorId;
        this.subjectId = subjectId;
        this.classNo = classNo;
        this.subjectType = subjectType;
        this.professorNo = professorNo;
        this.credit = credit;
    }

    public String toString() {
        //
        return toJson();
    }

    public static LectureOpening fromJson(String json) {
        //
        return JsonUtil.fromJson(json, LectureOpening.class);
    }

    public void setValues(NameValueList nameValues) {
        //
        for(NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();
            switch (nameValue.getName()) {
                case "professorNo":
                    this.professorNo = value;
                    break;
                case "credit":
                    this.credit = Integer.valueOf(value);
                    break;

                default:
                    throw new IllegalArgumentException("Update not allowed: " + nameValue);
            }
        }
    }

    public static LectureOpening sample() {
        //
        String schoolYear   = "2019";
        String departmentId = "A001";
        String majorId      = "M001";
        String subjectId    = "S001";
        String classNo      = "M102221";
        String professorNo  = "152468";
        int credit          = 3;

        LectureOpening sample = new LectureOpening(
                schoolYear
                , SemesterType.First
                ,departmentId
                ,majorId
                ,subjectId
                ,classNo
                , SubjectType.MajorNecessary
                ,professorNo
                ,credit
        );

        return sample;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}