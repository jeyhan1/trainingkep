package kookmin.uni.sugang.lectureopen.domain.store;

import kookmin.uni.sugang.lectureopen.domain.entity.LectureOpening;

public interface LectureOpenStore {
    //
    void createLectureOpen(LectureOpening lectureOpening);
    LectureOpening retrieveLectureOpen(String id);
    void updateLectureOpen(LectureOpening lectureOpening);
    void deleteLectureOpen(LectureOpening lectureOpening);
    boolean existsBySubjectId(String subjectId);
}