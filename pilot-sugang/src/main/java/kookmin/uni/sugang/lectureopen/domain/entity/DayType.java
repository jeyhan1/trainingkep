package kookmin.uni.sugang.lectureopen.domain.entity;

public enum DayType {
    Monday("1"),
    Tuesday("2"),
    Wednesday("3"),
    Thursday("4"),
    Friday("5"),
    Saturday("6"),
    Sunday("7");

    private String code;

    DayType(String code) {
        //
        this.code = code;
    }

    public String code() {
        //
        return code;
    }

    public DayType fromCode(String code) {
        //
        return null;
    }

    public boolean equalsFromCode(String code) {
        //
        return false;
    }
}
