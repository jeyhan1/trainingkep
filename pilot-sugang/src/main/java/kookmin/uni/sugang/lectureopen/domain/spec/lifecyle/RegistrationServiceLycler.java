package kookmin.uni.sugang.lectureopen.domain.spec.lifecyle;

import kookmin.uni.sugang.lectureopen.domain.spec.crud.LectureOpenService;

public interface RegistrationServiceLycler {
    //
    LectureOpenService requestLectureOpenService();
}
