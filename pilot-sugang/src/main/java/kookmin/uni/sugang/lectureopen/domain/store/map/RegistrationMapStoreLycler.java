package kookmin.uni.sugang.lectureopen.domain.store.map;

import kookmin.uni.sugang.lectureopen.domain.store.lifecycle.RegistrationStoreLycler;
import kookmin.uni.sugang.lectureopen.domain.store.LectureOpenStore;

public class RegistrationMapStoreLycler  implements RegistrationStoreLycler {
    //
    private static RegistrationStoreLycler singletonStoreLycler;

    private LectureOpenStore lectureOpenStore;

    protected RegistrationMapStoreLycler() {
        //
        this.lectureOpenStore = new LectureOpenMapStore();
    }

    public static RegistrationStoreLycler getInstance() {
        //
        if(singletonStoreLycler == null) {
            singletonStoreLycler = new RegistrationMapStoreLycler();
        }

        return singletonStoreLycler;
    }

    public static RegistrationStoreLycler newInstance() {
        //
        return new RegistrationMapStoreLycler();
    }

    @Override
    public LectureOpenStore requestLectureOpenStore() {
        return lectureOpenStore;
    }
}
