package kookmin.uni.sugang.lectureopen.domain.spec.crud;

import io.naraplatform.share.domain.NameValueList;
import kookmin.uni.sugang.lectureopen.domain.entity.LectureOpening;
import kookmin.uni.sugang.lectureopen.domain.spec.crud.sdo.LectureOpeningCdo;

public interface LectureOpenService {
    //
    String registerLectureOpen(LectureOpeningCdo lectureOpeningCdo);
    LectureOpening findLectureOpen(String lectureId);
    void modifyLectureOpen(String lectureId, NameValueList nameNames);
    void removeLectureOpen(String lectureId);
}