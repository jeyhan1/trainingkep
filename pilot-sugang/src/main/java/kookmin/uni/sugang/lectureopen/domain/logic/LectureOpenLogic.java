package kookmin.uni.sugang.lectureopen.domain.logic;

import io.naraplatform.share.domain.NameValueList;
import io.naraplatform.share.exception.store.AlreadyExistsException;
import kookmin.uni.sugang.lectureopen.domain.entity.LectureOpening;
import kookmin.uni.sugang.lectureopen.domain.store.lifecycle.RegistrationStoreLycler;
import kookmin.uni.sugang.lectureopen.domain.spec.crud.LectureOpenService;
import kookmin.uni.sugang.lectureopen.domain.spec.crud.sdo.LectureOpeningCdo;
import kookmin.uni.sugang.lectureopen.domain.store.LectureOpenStore;

import java.util.NoSuchElementException;

public class LectureOpenLogic implements LectureOpenService {
    //
    private LectureOpenStore lectureOpenStore;


    public LectureOpenLogic(RegistrationStoreLycler storeLycler) {
        //
        this.lectureOpenStore = storeLycler.requestLectureOpenStore();
    }

    @Override
    public String registerLectureOpen(LectureOpeningCdo lectureOpeningCdo) {
        //
        String sid = lectureOpeningCdo.getSubjectId();
        if (lectureOpenStore.existsBySubjectId(sid)) {
            throw new AlreadyExistsException("Sid: " + sid);
        }

        LectureOpening lectureOpening = new LectureOpening(
                lectureOpeningCdo.getSchoolYear(),
                lectureOpeningCdo.getSemesterType(),
                lectureOpeningCdo.getDepartmentId(),
                lectureOpeningCdo.getMajorId(),
                lectureOpeningCdo.getSubjectId(),
                lectureOpeningCdo.getClassNo(),
                lectureOpeningCdo.getSubjectType(),
                lectureOpeningCdo.getProfessorNo(),
                lectureOpeningCdo.getCredit()
        );

        lectureOpenStore.createLectureOpen(lectureOpening);

        return lectureOpening.getId();
    }

    @Override
    public LectureOpening findLectureOpen(String lectureId) {
        //
        LectureOpening lectureOpening = lectureOpenStore.retrieveLectureOpen(lectureId);
        if (lectureOpening == null) {
            throw new NoSuchElementException("LectureOffering id: " + lectureId);
        }

        return lectureOpening;
    }

    @Override
    public void modifyLectureOpen(String lectureId, NameValueList nameNames) {
        //
        LectureOpening lectureOpening = findLectureOpen(lectureId);
        lectureOpening.setValues(nameNames);

        lectureOpenStore.updateLectureOpen(lectureOpening);
    }

    @Override
    public void removeLectureOpen(String lectureId) {
        //
        LectureOpening lectureOpening = findLectureOpen(lectureId);
        //
        // do something

        lectureOpenStore.deleteLectureOpen(lectureOpening);
    }
}