package kookmin.uni.sugang.lectureopen.domain.store.lifecycle;

import kookmin.uni.sugang.lectureopen.domain.store.LectureOpenStore;

public interface RegistrationStoreLycler {
    //
    LectureOpenStore requestLectureOpenStore();
}