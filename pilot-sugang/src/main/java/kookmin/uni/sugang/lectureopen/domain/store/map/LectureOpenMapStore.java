package kookmin.uni.sugang.lectureopen.domain.store.map;

import kookmin.uni.sugang.lectureopen.domain.entity.LectureOpening;
import kookmin.uni.sugang.lectureopen.domain.store.LectureOpenStore;

import java.util.LinkedHashMap;
import java.util.Map;

public class LectureOpenMapStore implements LectureOpenStore {
    //
    private Map<String, LectureOpening> lectureOpeningMap;

    public LectureOpenMapStore() {
        //
        this.lectureOpeningMap = new LinkedHashMap<>();
    }

    @Override
    public void createLectureOpen(LectureOpening lectureOpening) {
        //
        lectureOpeningMap.put(lectureOpening.getId(), lectureOpening);
    }

    @Override
    public LectureOpening retrieveLectureOpen(String id) {
        //
        return lectureOpeningMap.get(id);
    }

    @Override
    public void updateLectureOpen(LectureOpening lectureOpening) {
        //
        // do nothing
    }

    @Override
    public void deleteLectureOpen(LectureOpening lectureOpening) {
        //
        lectureOpeningMap.remove(lectureOpening.getId());
    }

    @Override
    public boolean existsBySubjectId(String subjectId) {
        for (LectureOpening lectureOpening : lectureOpeningMap.values()) {
            if (lectureOpening.getSubjectId().equals(subjectId)) {
                return true;
            }
        }
        return false;
    }
}
