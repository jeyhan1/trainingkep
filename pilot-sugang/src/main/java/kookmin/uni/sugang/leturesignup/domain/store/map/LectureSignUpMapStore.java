package kookmin.uni.sugang.leturesignup.domain.store.map;

import kookmin.uni.sugang.leturesignup.domain.entity.LectureSignUp;
import kookmin.uni.sugang.leturesignup.domain.store.LectureSignUpStore;

import java.util.LinkedHashMap;
import java.util.Map;

public class LectureSignUpMapStore implements LectureSignUpStore {
    //
    private Map<String, LectureSignUp> lectureSignUpMap;

    public LectureSignUpMapStore() {
        //
        this.lectureSignUpMap = new LinkedHashMap<>();
    }

    @Override
    public void createLectureSignUp(LectureSignUp lectureSignUp) {
        //
        lectureSignUpMap.put(lectureSignUp.getId(), lectureSignUp);
    }

    @Override
    public LectureSignUp retrieveLectureSignUp(String id) {
        //
        return lectureSignUpMap.get(id);
    }

    @Override
    public void updateLectureSignUp(LectureSignUp lectureSignUp) {
        //
        // do nothing
    }

    @Override
    public void deleteLectureSignUp(LectureSignUp lectureSignUp) {
        //
        lectureSignUpMap.remove(lectureSignUp.getId());
    }

    @Override
    public boolean existsBySubjectId(String subjectId) {
        for (LectureSignUp lectureSignUp : lectureSignUpMap.values()) {
            if (lectureSignUp.getSubjectId().equals(subjectId)) {
                return true;
            }
        }
        return false;
    }
}
