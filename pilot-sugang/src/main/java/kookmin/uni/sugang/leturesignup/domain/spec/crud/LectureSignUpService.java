package kookmin.uni.sugang.leturesignup.domain.spec.crud;

import io.naraplatform.share.domain.NameValueList;
import kookmin.uni.sugang.leturesignup.domain.entity.LectureSignUp;
import kookmin.uni.sugang.leturesignup.domain.spec.crud.sdo.LectureSignUpCdo;


public interface LectureSignUpService {
    //
    String registerLectureSignUp(LectureSignUpCdo lectureSignUpCdo);
    LectureSignUp findLectureSignUp(String lectureId);
    void modifyLectureSignUp(String lectureId, NameValueList nameNames);
    void removeLectureSignUp(String lectureId);
}