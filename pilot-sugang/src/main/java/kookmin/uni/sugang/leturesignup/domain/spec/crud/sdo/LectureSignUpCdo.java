package kookmin.uni.sugang.leturesignup.domain.spec.crud.sdo;

import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import kookmin.uni.sugang.curriculum.domain.entity.SemesterType;
import kookmin.uni.sugang.curriculum.domain.entity.SubjectType;
import kookmin.uni.sugang.lectureopen.domain.entity.DayType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LectureSignUpCdo implements JsonSerializable {
    //
    private String schoolYear;
    private SemesterType semesterType;
    private String departmentId;
    private String majorId;
    private String subjectId;
    private String classNo;
    private String studentNo;
    private SubjectType subjectType;
    private String professorNo;
    private int credit;
    private String buildingId;
    private String roomNo;
    private DayType dayType;
    private String startTime;

    public String toString() {
        //
        return toJson();
    }

    public static LectureSignUpCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, LectureSignUpCdo.class);
    }

    public static LectureSignUpCdo sample() {
        //
        String schoolYear   = "2019";
        String departmentId = "A001";
        String majorId      = "M001";
        String subjectId    = "S001";
        String classNo      = "M102221";
        String studentNo    = "9006049";
        String professorNo  = "152468";
        int credit          = 3;
        String buildingId   = "DDD";
        String roomNo       = "219";
        String startTime = "10:00";

        LectureSignUpCdo sample = new LectureSignUpCdo(
                schoolYear
                ,SemesterType.First
                ,departmentId
                ,majorId
                ,subjectId
                ,classNo
                ,studentNo
                ,SubjectType.MajorNecessary
                ,professorNo
                ,credit
                ,buildingId
                ,roomNo
                ,DayType.Friday
                ,startTime
        );

        return sample;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
