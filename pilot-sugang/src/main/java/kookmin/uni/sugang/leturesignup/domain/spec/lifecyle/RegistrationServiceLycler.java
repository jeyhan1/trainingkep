package kookmin.uni.sugang.leturesignup.domain.spec.lifecyle;

import kookmin.uni.sugang.leturesignup.domain.spec.crud.LectureSignUpService;

public interface RegistrationServiceLycler {
    //
    LectureSignUpService requestEnrolmentService();
}
