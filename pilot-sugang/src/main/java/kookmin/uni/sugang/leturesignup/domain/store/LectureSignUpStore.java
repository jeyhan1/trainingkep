package kookmin.uni.sugang.leturesignup.domain.store;

import kookmin.uni.sugang.leturesignup.domain.entity.LectureSignUp;

public interface LectureSignUpStore {
    //
    void createLectureSignUp(LectureSignUp lectureSignUp);
    LectureSignUp retrieveLectureSignUp(String id);
    void updateLectureSignUp(LectureSignUp lectureSignUp);
    void deleteLectureSignUp(LectureSignUp lectureSignUp);
    boolean existsBySubjectId(String subjectId);
}