package kookmin.uni.sugang.leturesignup.domain.store.lifecycle;

import kookmin.uni.sugang.leturesignup.domain.store.LectureSignUpStore;

public interface RegistrationStoreLycler {
    //
    LectureSignUpStore requestLectureSignUpStore();
}