package kookmin.uni.sugang.leturesignup.domain.store.map;

import kookmin.uni.sugang.leturesignup.domain.store.LectureSignUpStore;
import kookmin.uni.sugang.leturesignup.domain.store.lifecycle.RegistrationStoreLycler;

public class RegistrationMapStoreLycler  implements RegistrationStoreLycler {
    //
    private static RegistrationStoreLycler singletonStoreLycler;

    private LectureSignUpStore lectureSignUpStore;

    protected RegistrationMapStoreLycler() {
        //
        this.lectureSignUpStore = new LectureSignUpMapStore();
    }

    public static RegistrationStoreLycler getInstance() {
        //
        if(singletonStoreLycler == null) {
            singletonStoreLycler = new RegistrationMapStoreLycler();
        }

        return singletonStoreLycler;
    }

    public static RegistrationStoreLycler newInstance() {
        //
        return new RegistrationMapStoreLycler();
    }

    @Override
    public LectureSignUpStore requestLectureSignUpStore() {
        return lectureSignUpStore;
    }
}
