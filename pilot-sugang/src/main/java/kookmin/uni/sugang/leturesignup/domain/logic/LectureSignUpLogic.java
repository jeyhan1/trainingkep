package kookmin.uni.sugang.leturesignup.domain.logic;

import io.naraplatform.share.domain.NameValueList;
import io.naraplatform.share.exception.store.AlreadyExistsException;
import kookmin.uni.sugang.leturesignup.domain.entity.LectureSignUp;
import kookmin.uni.sugang.leturesignup.domain.spec.crud.LectureSignUpService;
import kookmin.uni.sugang.leturesignup.domain.spec.crud.sdo.LectureSignUpCdo;
import kookmin.uni.sugang.leturesignup.domain.store.LectureSignUpStore;
import kookmin.uni.sugang.leturesignup.domain.store.lifecycle.RegistrationStoreLycler;

import java.util.NoSuchElementException;

public class LectureSignUpLogic implements LectureSignUpService {
    //
    private LectureSignUpStore lectureSignUpStore;


    public LectureSignUpLogic(RegistrationStoreLycler storeLycler) {
        //
        this.lectureSignUpStore = storeLycler.requestLectureSignUpStore();
    }

    @Override
    public String registerLectureSignUp(LectureSignUpCdo lectureSignUpCdo) {
        //
        String sid = lectureSignUpCdo.getSubjectId();
        if (lectureSignUpStore.existsBySubjectId(sid)) {
            throw new AlreadyExistsException("Sid: " + sid);
        }

        LectureSignUp lectureSignUp = new LectureSignUp(
                lectureSignUpCdo.getSchoolYear(),
                lectureSignUpCdo.getSemesterType(),
                lectureSignUpCdo.getDepartmentId(),
                lectureSignUpCdo.getMajorId(),
                lectureSignUpCdo.getSubjectId(),
                lectureSignUpCdo.getClassNo(),
                lectureSignUpCdo.getSubjectType(),
                lectureSignUpCdo.getProfessorNo(),
                lectureSignUpCdo.getCredit()
        );

        lectureSignUpStore.createLectureSignUp(lectureSignUp);

        return lectureSignUp.getId();
    }

    @Override
    public LectureSignUp findLectureSignUp(String lectureId) {
        //
        LectureSignUp lectureSignUp = lectureSignUpStore.retrieveLectureSignUp(lectureId);
        if (lectureSignUp == null) {
            throw new NoSuchElementException("LectureOffering id: " + lectureId);
        }

        return lectureSignUp;
    }

    @Override
    public void modifyLectureSignUp(String lectureId, NameValueList nameNames) {
        //
        LectureSignUp lectureSignUp = findLectureSignUp(lectureId);
        lectureSignUp.setValues(nameNames);

        lectureSignUpStore.updateLectureSignUp(lectureSignUp);
    }

    @Override
    public void removeLectureSignUp(String lectureId) {
        //
        LectureSignUp lectureSignUp = findLectureSignUp(lectureId);
        //
        // do something

        lectureSignUpStore.deleteLectureSignUp(lectureSignUp);
    }
}