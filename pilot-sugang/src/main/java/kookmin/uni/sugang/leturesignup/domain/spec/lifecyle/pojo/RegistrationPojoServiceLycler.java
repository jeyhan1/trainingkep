package kookmin.uni.sugang.leturesignup.domain.spec.lifecyle.pojo;

import kookmin.uni.sugang.leturesignup.domain.logic.LectureSignUpLogic;
import kookmin.uni.sugang.leturesignup.domain.spec.crud.LectureSignUpService;
import kookmin.uni.sugang.leturesignup.domain.spec.lifecyle.RegistrationServiceLycler;
import kookmin.uni.sugang.leturesignup.domain.store.lifecycle.RegistrationStoreLycler;
import kookmin.uni.sugang.leturesignup.domain.store.map.RegistrationMapStoreLycler;

public class RegistrationPojoServiceLycler implements RegistrationServiceLycler {
    //
    private static RegistrationServiceLycler singletonServiceLycler;

    private LectureSignUpService lectureSignUpService;

    public RegistrationPojoServiceLycler() {
        //
        RegistrationStoreLycler storeLycler = RegistrationMapStoreLycler.getInstance();
        this.lectureSignUpService = new LectureSignUpLogic(storeLycler);
    }

    public static RegistrationServiceLycler getInstance() {
        //
        if(singletonServiceLycler == null) {
            singletonServiceLycler = new RegistrationPojoServiceLycler();
        }

        return singletonServiceLycler;
    }

    public static RegistrationServiceLycler newInstance() {
        //
        return new RegistrationPojoServiceLycler();
    }

    @Override
    public LectureSignUpService requestEnrolmentService() {
        //
        return lectureSignUpService;
    }

 }
