**gitlab 계정 생성이후 local(pc) git local repository 환경설정 **
 
 1) c:\project 폴더 아래에서 (폴더명은 각 개인이 원하시는대로..)

    git config --global user.email "이메일 주소"
     
    git config --list    :  config/전역설정 확인  : 메일주소 맞는지 확인
    git config --global --list    
     
    git config --global color.ui "auto"  :  - 콘솔에서 git output을 컬러로 출력
                                            - commit 변경사항을 빅하는 diff 할떄 유용
                                            - color.ui=auto 가 config list 에 새롭게 추가됩니다.
 
  2) 초기 프로젝트  다운로드
    c:\project 폴더 아래에서
    git clone https://gitlab.com/jeyhan1/trainingkep.git

		c:\project\trainingkep 생성,소스 다운로드     
     
  3) c:\project\trainingkep\ 아래 소스 다운로드  확인된후
  
		intellij 에서 pilot-sugang, share_master 각각 오픈
  	share_master의 Maven 탭의 세개를 모두 install
  	pilot-sugang의 Maven 탭에서 install

** source 형상 upload/download 명령어  **

git add . (all *.* 의미)  
git add <파일명>
  --> local rep 에 파일 추가 (추적파일알림) 변경된 파일만 추가 

git status  --> local 작업 상태 확인:add/commit 등

git commit -a -m "커밋내용"    -> local rep 에 모든 파일 commit 

git revert  --> commit 취소  (삭제가 아니라 이전버전과 같은 내용으로 커밋))

git log     --> commit 내용 확인

git pull    -->  remote (https://gitlab.com/jeyhan1/trainingkep) download 

git push 		--> remote upload : pwd로 디렉토리 확인하여 project master 위치에서 진행 
																push  하기전에 항상 git pull 해서 다른유저가 변경해서 올려놓은 내용이 있는지 확인

