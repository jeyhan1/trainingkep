package io.naraplatform.share.util.locale;

public class PhoneNumberUtil {
    //
    /**
     * Return country phone code.
     * <pre>
     * ex) KR == 82
     * </pre>
     *
     * @param alpha2Code phone
     * @return country code for phone
     */
    public static int getCountryCode(String alpha2Code) {
        //
        com.google.i18n.phonenumbers.PhoneNumberUtil phoneNumberUtil = com.google.i18n.phonenumbers.PhoneNumberUtil.getInstance();
        return phoneNumberUtil.getCountryCodeForRegion(alpha2Code);
    }

}
