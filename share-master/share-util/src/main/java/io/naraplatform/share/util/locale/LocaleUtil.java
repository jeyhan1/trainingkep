package io.naraplatform.share.util.locale;

import io.naraplatform.share.exception.NaraException;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class LocaleUtil {
    //
    private static List<String> isoCountries = Arrays.asList(Locale.getISOCountries());

    /**
     *
     * @param countryCode ISO 3166 alpha-2 code.
     * @return true if countryCode is valid
     */
    public static boolean isValidCountryCode(String countryCode) {
        //
        if (countryCode == null) throw new NaraException("Country code is null.");
        if (countryCode.length() != 2) throw new NaraException("Country code must be ISO 3166 alpha-2 code.");
        return isoCountries.contains(countryCode);
    }

    public static boolean isValidLocale(final String value) {
        //
        Locale[] locales = Locale.getAvailableLocales();
        for (Locale locale : locales) {
            if (value.equals(locale.toString())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isValidLanguage(final String langCode) {
        //
        Locale[] locales = Locale.getAvailableLocales();
        for (Locale locale : locales) {
            if (locale.getLanguage().equals(langCode)) {
                return true;
            }
        }

        return false;
    }

    public static Locale parseLocale(final String locale) {
        //
        final String[] localeElements = locale.split("_");
        switch (localeElements.length) {
            case 1:
                return new Locale(localeElements[0]);
            case 2:
                return new Locale(localeElements[0], localeElements[1]);
            case 3:
                return new Locale(localeElements[0], localeElements[1], localeElements[2]);
            default:
                throw new IllegalArgumentException("Invalid locale format;");
        }
    }
}
