package io.naraplatform.share.util.object;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class ObjectUtil {
    //
    /** logger */
    private static Log logger = LogFactory.getLog(ObjectUtil.class);

    /**
     * Returns a deepCopy of the object, or null if the object cannot
     * be serialized.
     *
     * @param obj converting object
     * @return converted map
     */
    public static Map convertObjectToMap(Object obj) {
        Map map = new HashMap();
        Field[] fields = obj.getClass().getDeclaredFields();
        for(int i=0; i <fields.length; i++){
            fields[i].setAccessible(true);
            try{
                map.put(fields[i].getName(), fields[i].get(obj));
            }catch(Exception e){
                logger.error("Object converting errro", e);
            }
        }
        return map;
    }

}
