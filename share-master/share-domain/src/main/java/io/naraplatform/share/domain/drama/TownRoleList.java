package io.naraplatform.share.domain.drama;

import io.naraplatform.share.domain.ValueObject;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Getter
@Setter
public class TownRoleList implements ValueObject {
    //
    private List<TownRole> roles;

    public TownRoleList() {
        //
        this.roles = new ArrayList<>();
    }

    public TownRoleList(TownRole role) {
        //
        this();
        this.roles.add(role);
    }

    public TownRoleList(List<TownRole> roles) {
        //
        this();
        this.roles.addAll(roles);
    }

    public static TownRoleList defaultRoles() {
        //
        return new TownRoleList(TownRole.defaultRoles());
    }

    public String toString() {
        //
        return toJson();
    }

    public static TownRoleList fromJson(String json) {
        //
        return JsonUtil.fromJson(json, TownRoleList.class);
    }

    public static TownRoleList sample() {
        //
        return defaultRoles();
    }

    public int size() {
        //
        return roles.size();
    }

    public boolean hasRole() {
        //
        if(size() > 0) {
            return true;
        }

        return false;
    }

    public TownRole getOne(int index) {
        //
        if (!hasRole()) {
            throw new NoSuchElementException("No role.");
        }

        return roles.get(index);
    }

    public void addAll(List<TownRole> roles) {
        //
        this.roles.addAll(roles);
    }

    public TownRoleList add(TownRole role) {
        //
        this.roles.add(role);
        return this;
    }

    public TownRole getByKey(String key) {
        //
        if(!hasRole()) {
            throw new NoSuchElementException("No role.");
        }

        TownRole foundRole = null;

        for(TownRole role : this.roles) {
            if (role.getKey().equals(key)) {
                foundRole = role;
                break;
            }
        }

        if (foundRole == null) {
            throw new NoSuchElementException("Key: " + key);
        }

        return foundRole;
    }

    public TownRole getByName(String roleName) {
        //
        if(!hasRole()) {
            throw new NoSuchElementException("No role.");
        }

        TownRole foundRole = null;

        for(TownRole role : this.roles) {
            if (role.getLangNames().getStrings().contains(roleName)) {
                foundRole = role;
                break;
            }
        }

        if (foundRole == null) {
            throw new NoSuchElementException("Role name: " + roleName);
        }

        return foundRole;
    }

    public boolean hasByName(String roleName) {
        //
        try {
            getByName(roleName);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean hasDefaultUserRole() {
        //
        if (getDefaultUserRole()  != null) {
            return true;
        }

        return false;
    }

    public TownRole getDefaultUserRole() {
        //
        for(TownRole role : roles) {
            if (role.isDefaultUserRole()) {
                return role;
            }
        }

        return null;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
