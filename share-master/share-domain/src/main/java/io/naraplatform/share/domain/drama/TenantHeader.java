package io.naraplatform.share.domain.drama;

import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class TenantHeader implements JsonSerializable {
    //
    private String subscriptionId;
    private String organizationId;            // square/pavilion/cineroom

    public TenantHeader(String subscriptionId, String organizationId) {
        //
        this.subscriptionId = subscriptionId;
        this.organizationId = organizationId;
    }

    public String toString() {
        //
        return toJson();
    }

    public static TenantHeader fromJson(String json) {
        //
        return JsonUtil.fromJson(json, TenantHeader.class);
    }

    public static TenantHeader sample() {
        //
        String subscriptionId = UUID.randomUUID().toString();
        String organizationId = "M1S1P1C1";

        TenantHeader sample = new TenantHeader(subscriptionId, organizationId);

        return sample;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
