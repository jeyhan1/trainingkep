package io.naraplatform.share.domain;

import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;
import java.util.StringTokenizer;

@Getter
@Setter
@NoArgsConstructor
public class CodeName implements ValueObject {
    //
    private String code;
    private String name;

    public CodeName(String code, String name) {
        //
        this.code = code;
        this.name = name;
    }

    public String toString() {
        //
        return toJson();
    }

    public String toSimpleString() {
        //
        return code + ":" + name;
    }

    public static CodeName fromString(String idNameStr) {
        //
        StringTokenizer tokenizer = new StringTokenizer(idNameStr, ":");
        String id = tokenizer.nextToken();
        String name = tokenizer.nextToken();

        return new CodeName(id, name);
    }

    public static CodeName sample() {
        //
        String code = "ABCD";
        String name = "Crane";

        return new CodeName(code, name);
    }

    public static CodeName fromJson(String json) {
        //
        return JsonUtil.fromJson(json, CodeName.class);
    }

    @Override
    public boolean equals(Object target) {
        //
        if (this == target) {
            return true;
        }

        if (target == null || getClass() != target.getClass()) {
            return false;
        }

        CodeName idName = (CodeName) target;

        return Objects.equals(code, idName.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
        System.out.println(CodeName.fromString(sample().toSimpleString()));
    }
}
