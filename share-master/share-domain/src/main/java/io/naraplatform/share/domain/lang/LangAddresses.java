package io.naraplatform.share.domain.lang;

import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class LangAddresses implements JsonSerializable {
    //
    private List<LangAddress> addresses;

    public LangAddresses() {
        //
        this.addresses = new ArrayList<>();
    }

    private LangAddresses(LangAddress langAddress) {
        //
        this();
        this.addresses.add(langAddress);
    }

    public static LangAddresses newAddress(LangAddress langAddress) {
        //
        return new LangAddresses(langAddress);
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static LangAddresses fromJson(String json) {
        //
        return JsonUtil.fromJson(json, LangAddresses.class);
    }

    public static LangAddresses sample() {
        //
        LangAddresses sample = new LangAddresses(LangAddress.sample());
        sample.add(LangAddress.secondSample());

        return sample;
    }

    public int size() {
        //
        return addresses.size();
    }

    public List<LangAddress> list() {
        //
        return this.addresses;
    }

    public List<LangAddress> addresses() {
        //
        return this.addresses;
    }

    public LangAddresses add(LangAddress langAddress) {
        //
        if(langAddress == null) {
            return this;
        }

        addresses.add(langAddress);

        return this;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
