package io.naraplatform.share.domain;

import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;
import java.util.StringTokenizer;

@Getter
@Setter
@NoArgsConstructor
public class IdName implements ValueObject {
    //
    private String id;
    private String name;

    public IdName(String id, String name) {
        //
        this.id = id;
        this.name = name;
    }

    public String toString() {
        //
        return toJson();
    }

    public String toSimpleString() {
        //
        return id + ":" + name;
    }

    public static IdName fromString(String idNameStr) {
        //
        StringTokenizer tokenizer = new StringTokenizer(idNameStr, ":");
        String id = tokenizer.nextToken();
        String name = tokenizer.nextToken();

        return new IdName(id, name);
    }

    public static IdName sample() {
        //
        String id = "1234";
        String name = "Hansoo Lee";

        return new IdName(id, name);
    }

    public static IdName fromJson(String json) {
        //
        return JsonUtil.fromJson(json, IdName.class);
    }

    @Override
    public boolean equals(Object target) {
        //
        if (this == target) {
            return true;
        }

        if (target == null || getClass() != target.getClass()) {
            return false;
        }

        IdName idName = (IdName) target;

        return Objects.equals(id, idName.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
        System.out.println(IdName.fromString(sample().toSimpleString()));
    }
}
