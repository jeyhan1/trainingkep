package io.naraplatform.share.domain.lang;

import io.naraplatform.share.domain.ValueObject;
import io.naraplatform.share.domain.granule.Name;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Locale;

@Getter
@Setter
@NoArgsConstructor
public class LangName implements ValueObject {
    //
    private String language;
    private Name name;

    public LangName(String language, Name name) {
        //
        this.language = language;
        this.name = name;
    }

    public LangName(Locale locale, Name name) {
        //
        this.language = locale.getLanguage();
        this.name = name;
    }

    public static LangName newGivenFirst(String language, String givenName, String familyName) {
        //
        return new LangName(language, Name.newGivenFirst(givenName, familyName));
    }

    public static LangName newFamilyFirst(String language, String familyName, String givenName) {
        //
        return new LangName(language, Name.newFamilyFirst(familyName, givenName));
    }

    public static LangName sample() {
        //
        Locale locale = Locale.US;
        Name name = Name.sample();

        LangName sample = new LangName(locale, name);

        return sample;
    }

    public static LangName secondSample() {
        //
        Locale langLocale = Locale.KOREA;
        Name name = Name.sampleFamilyFirst();

        LangName sample = new LangName(langLocale, name);

        return sample;
    }

    public static LangName fromJson(String json) {
        //
        return JsonUtil.fromJson(json, LangName.class);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toJson());
        System.out.println(secondSample().toJson());
    }
}
