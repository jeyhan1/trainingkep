package io.naraplatform.share.event;

public enum EventType {
    //
    Created,
    Updated,
    Deleted,
    ChildAdded,
    ChildRemoved,
    ChildUpdated,
    StateChanged,
    None;
}
