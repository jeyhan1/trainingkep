package io.naraplatform.share.domain.granule;

import io.naraplatform.share.domain.NameValueList;
import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SimpleContact implements JsonSerializable {
    //
    private String email;
    private String phone;

    private SimpleContact(String email, String phone) {
        //
        this.email = email;
        this.phone = phone;
    }

    public String toString() {
        //
        return toJson();
    }

    public static SimpleContact newEmailPhone(String email, String phone) {
        //
        return new SimpleContact(email, phone);
    }

    public static SimpleContact sample() {
        //
        String email = Email.sample().getEmail();
        String phone = Phone.mobileSample().getDisplayNumber();

        SimpleContact sample = new SimpleContact(email, phone);

        return sample;
    }

    public static SimpleContact fromJson(String json) {
        //
        return JsonUtil.fromJson(json, SimpleContact.class);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
