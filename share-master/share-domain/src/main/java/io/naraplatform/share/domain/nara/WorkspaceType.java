package io.naraplatform.share.domain.nara;

public enum WorkspaceType {
    //
    Station,
    Square,
    Pavilion,
    Cineroom,
    Studio
}