package io.naraplatform.share.domain;

import io.naraplatform.share.util.json.JsonSerializable;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class IntPairList implements JsonSerializable {
	//
	private List<IntPair> intPairs;

	public IntPairList() {
	    //
        this.intPairs = new ArrayList<IntPair>();
	}

	public IntPairList(IntPair intPair) {
	    //
	    this();
	    intPairs.add(intPair);
    }

    public IntPairList(int left, int right) {
	    //
        this();
        intPairs.add(new IntPair(left, right));
    }

	private IntPairList(int leftCount) {
		//
        this();
		for(int i=0; i<leftCount; i++) {
			intPairs.add(new IntPair(i+1, 0));
		}
	}

	public static IntPairList buildPairSequence(int leftCount) {
	    //
        return new IntPairList(leftCount);
    }

	@Override
	public String toString() {
		//
        return toJson();
	}

	public static IntPairList sample() {
		//
		IntPairList sample = IntPairList.buildPairSequence(5);
		sample.increaseRightOf(1);
		sample.increaseRightOf(2);
		sample.increaseRightOf(3);
		sample.increaseRightOf(4);
		sample.increaseRightOf(5);

		return sample;
	}

	public void increaseRightOf(int left) {
		//
		intPairs.get(left-1).increaseRight();
	}

	public void decreaseRightOf(int left) {
		//
		intPairs.get(left-1).decreaseRight();
	}

	public List<IntPair> list() {
		//
		return intPairs;
	}

	public boolean containsRight(int left) {
		//
		for(IntPair intPair : this.intPairs) {
			if (intPair.getLeft() == left) {
				return true;
			}
		}
		return false;
	}

	public int size() {
		return intPairs.size();
	}

	public static void main(String[] args) {
		//
		System.out.println(sample());
	}
}
