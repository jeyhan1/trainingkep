package io.naraplatform.share.domain.granule;

import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BizIdentity implements JsonSerializable {
    //
    private String bizType;             // 업종
    private String bizCategory;         // 업태
    private String bizRegCertificate;   // 사업자 등록번호
    private String companyRegNumber;    // 법인 등록번호

    public BizIdentity(String bizType,
                       String bizCategory,
                       String bizRegCertificate,
                       String companyRegNumber) {
        //
        this.bizType = bizType;
        this.bizCategory = bizCategory;
        this.bizRegCertificate = bizRegCertificate;
        this.companyRegNumber = companyRegNumber;
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static BizIdentity sample() {
        //
        String bizType = "서비스";
        String bizCategory = "온라인 Java교육";
        String bizRegCertificate = "123-45-67891";
        String companyRegNumber = "110111-006243";

        BizIdentity sample = new BizIdentity(bizType, bizCategory, bizRegCertificate, companyRegNumber);

        return sample;
    }

    public static BizIdentity fromJson(String json) {
        //
        return JsonUtil.fromJson(json, BizIdentity.class);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
