package io.naraplatform.share.domain.granule;

import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class NaraFile implements JsonSerializable {
    //
    private String diskFileId;
    private String name;
    private String contentType;
    private Long size;
    private Long time;

    public NaraFile(String name, String contentType, Long size) {
        //
        this.name = name;
        this.contentType = contentType;
        this.size = size;
        this.time = System.currentTimeMillis();
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static NaraFile sample() {
        //
        String name = "Hello.java";
        String contentType = "text/plain";
        Long size = 120L;

        NaraFile sample = new NaraFile(name, contentType, size);

        return sample;
    }

    public static NaraFile fromJson(String json) {
        //
        return JsonUtil.fromJson(json, NaraFile.class);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
