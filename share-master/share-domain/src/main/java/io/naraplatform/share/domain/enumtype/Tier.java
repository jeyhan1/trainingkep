package io.naraplatform.share.domain.enumtype;

public enum Tier {
    //
	Primary,
    Secondary,
    Third;

    public boolean isPrimary() {
        //
        return this.equals(Primary);
    }

    public boolean isSecondary() {
        //
        return this.equals(Secondary);
    }

    public boolean isThird() {
        //
        return this.equals(Third);
    }

    public static void main(String[] args) {
        //
        Tier tier = Tier.Primary;
        System.out.println(tier.isPrimary());
    }
    
}