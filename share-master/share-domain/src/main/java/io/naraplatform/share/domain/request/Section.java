package io.naraplatform.share.domain.request;

import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Section implements JsonSerializable {
    //
    private int offset;
    private int limit;

    public String toString() {
        //
        return toJson();
    }

    public static Section fromJson(String json) {
        //
        return JsonUtil.fromJson(json, Section.class);
    }

    public static Section sample() {
        //
        return new Section(0, 50);
    }
}