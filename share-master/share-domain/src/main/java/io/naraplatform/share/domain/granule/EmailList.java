package io.naraplatform.share.domain.granule;

import io.naraplatform.share.domain.enumtype.Tier;
import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;

import java.util.ArrayList;
import java.util.List;

public class EmailList implements JsonSerializable {
    //
    private List<Email> emails;

    public EmailList() {
        //
        this.emails = new ArrayList<>();
    }

    public EmailList(String email) {
        //
        this();
        this.add(Email.newOfficialPrimary(email));
    }

    public static EmailList list() {
        //
        return new EmailList();
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static EmailList sample() {
        //
        EmailList sample = new EmailList();
        sample.add(Email.sample());

        return sample;
    }

    public static EmailList fromJson(String json) {
        //
        return JsonUtil.fromJson(json, EmailList.class);
    }

    public EmailList add(Email email) {
        //
        if(emails.size() == 0) {
            email.setTier(Tier.Primary);
        } else if(email.isPrimary()) {
            Email primary = getPrimary();
            if (primary != null) {
                primary.setTier(Tier.Secondary);
            }
        }

        this.emails.add(email);

        return this;
    }

    public String getPrimaryEmail() {
        //
        Email primary = getPrimary();
        if (primary == null) return null;
        return primary.getEmail();
    }

    public Email getPrimary() {
        //
        for(Email email : emails) {
            //
            if (email.isPrimary()) {
                return email;
            }
        }

        return null;
    }

    public void addAll(List<Email> emails) {
        //
        for(Email email : emails) {
            this.add(email);
        }
    }

    public Email getFirst() {
        //
        if(emails == null|| emails.isEmpty())
            return null;

        return emails.get(0);
    }

    public Email get(String targetEmail) {
        //
        Email foundEmail = null;
        for(Email email : this.emails) {
            if (email.getEmail().equals(targetEmail)) {
                foundEmail = email;
                break;
            }
        }

        return foundEmail;
    }

    public boolean contains(String targetEmail) {
        //
        for(Email email : this.emails) {
            if (email.getEmail().equals(targetEmail)) {
                return true;
            }
        }
        return false;
    }

    public int size() {
        return emails.size();
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
