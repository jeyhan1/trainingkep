package io.naraplatform.share.domain.granule;

import io.naraplatform.share.domain.enumtype.Tier;
import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TieredEmail implements JsonSerializable {
    //
    private Category category;
    private Tier tier;
    //@Email                  // bean validation
    private String email;

    public TieredEmail(String email) {
        //
        this.tier = Tier.Secondary;
        this.email = email;
        this.category = Category.Others;
    }

    public TieredEmail(Category category, Tier tier, String email) {
        //
        this.category = category;
        this.tier = tier;
        this.email = email;
    }

    public static TieredEmail asOrgPrimary(String email) {
        //
        return new TieredEmail(Category.Organization, Tier.Primary, email);
    }

    public static TieredEmail asPersonalPrimary(String email) {
        //
        return new TieredEmail(Category.Personal, Tier.Primary, email);
    }

    public static TieredEmail fromJson(String json) {
        return JsonUtil.fromJson(json, TieredEmail.class);
    }

    public static TieredEmail sample() {
        //
        String email = "jhone@company.com";
        return asOrgPrimary(email);
    }

    public boolean isPrimary() {
        //
        if (this.tier.isPrimary()) {
            return true;
        }

        return false;
    }

    public enum Category {
        //
        Organization,
        Company,
        Personal,
        Others
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
