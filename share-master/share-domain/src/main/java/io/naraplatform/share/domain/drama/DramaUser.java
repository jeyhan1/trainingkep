package io.naraplatform.share.domain.drama;

import io.naraplatform.share.domain.IdName;
import io.naraplatform.share.domain.ValueObject;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class DramaUser implements ValueObject {
    //
    private String orderId;         // subscriptionId;
    private IdName user;            // citizenId,name;

    public DramaUser(String orderId, IdName user) {
        //
        this.orderId = orderId;
        this.user = user;
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static DramaUser fromJson(String json) {
        //
        return JsonUtil.fromJson(json, DramaUser.class);
    }

    public static DramaUser sample() {
        //
        String orderId = UUID.randomUUID().toString();
        IdName user = new IdName(UUID.randomUUID().toString(), "Dongsoo Kim");

        return new DramaUser(orderId, user);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
