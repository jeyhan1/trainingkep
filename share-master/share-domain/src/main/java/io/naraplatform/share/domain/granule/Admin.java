package io.naraplatform.share.domain.granule;

import io.naraplatform.share.domain.ValueObject;
import io.naraplatform.share.domain.enumtype.Tier;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Admin implements ValueObject {
    ////
    private Tier tier;
    private String usid;              // usid
    private String displayName;            // with default locale
    private String email;           // primary email
    private String phone;           // Optional

    private Admin(String usid, String displayName, String email) {
        //
        this.usid = usid;
        this.displayName = displayName;
        this.email = email;
    }

    public static Admin primary(String usid, String displayName, String email) {
        //
        Admin admin = new Admin(usid, displayName, email);
        admin.setTier(Tier.Primary);

        return admin;
    }

    public static Admin secondary(String usid, String displayName, String email) {
        //
        Admin admin = new Admin(usid, displayName, email);
        admin.setTier(Tier.Secondary);

        return admin;
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static Admin primarySample() {
        //
        String usid = "19-0909882";
        String displayName = "Steve Jobs";
        String email = "steve@apple.com";

        Admin sample = Admin.primary(usid, displayName, email);
        sample.setPhone("123-1234-1234");

        return sample;
    }

    public static Admin secondarySample() {
        //
        String usid = "20-1019293";
        String displayName = "Tim Cook";
        String email = "tim@apple.com";

        Admin sample = Admin.secondary(usid, displayName, email);
        sample.setPhone("123-1234-1234");

        return sample;
    }

    public static Admin fromJson(String json) {
        //
        return JsonUtil.fromJson(json, Admin.class);
    }

    public static void main(String[] args) {
        //
        System.out.println(primarySample());
        System.out.println(secondarySample());
    }
}
