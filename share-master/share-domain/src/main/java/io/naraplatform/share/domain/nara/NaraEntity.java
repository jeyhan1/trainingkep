package io.naraplatform.share.domain.nara;

import io.naraplatform.share.util.json.JsonSerializable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;
import java.util.UUID;

public abstract class NaraEntity implements JsonSerializable {
	//
	private final String id;
	private long entityVersion;

	protected NaraEntity() {
	    //
		this.id = UUID.randomUUID().toString();
	}

	protected NaraEntity(String id) {
	    //
		this.id = id;
	}

	protected NaraEntity(NaraEntity naraEntity) {
	    //
        this.id = naraEntity.getId();
        this.entityVersion = naraEntity.getEntityVersion();
    }

	public String getId() {
		return id;
	}

	@Override
	public boolean equals(Object target) {
		//
		if (this == target) {
			return true;
		}

		if (target == null || getClass() != target.getClass()) {
			return false;
		}

		NaraEntity entity = (NaraEntity) target;

		return Objects.equals(id, entity.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public String toString() {
	    //
		return JsonSerializable.super.toJson();
	}

	public long getEntityVersion() {
	    //
        return entityVersion;
    }

    public void setEntityVersion(long entityVersion) {
	    //
        this.entityVersion = entityVersion;
    }

    public void increaseEntityVersion() {
	    //
        this.entityVersion++;
    }

    public void decreaseEntityVersion() {
	    //
        this.entityVersion--;
    }

	public static void main(String[] args) {
	    //
        SampleEntity sampleEntity = new SampleEntity();
        sampleEntity.increaseEntityVersion();
        sampleEntity.setMemo("Hello");

        System.out.println(sampleEntity);
    }
}

@Getter
@Setter
@NoArgsConstructor
class SampleEntity extends NaraEntity {
    //
    private String memo;
}
