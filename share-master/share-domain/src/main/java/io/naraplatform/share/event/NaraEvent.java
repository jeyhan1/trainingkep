package io.naraplatform.share.event;

import io.naraplatform.share.util.json.JsonSerializable;

import java.util.Objects;
import java.util.UUID;

public abstract class NaraEvent implements JsonSerializable {
    //
    private final String id;
    private final String name;
    private final EventType type;
    private Long version;

    protected NaraEvent() {
        //
        this(EventType.None, "none");
    }

    protected NaraEvent(EventType type, String name) {
        //
        this.id = UUID.randomUUID().toString();
        this.type = type;
        this.name = name;
        this.version = 0L;
    }

    protected NaraEvent(String id, EventType type, String name) {
        //
        this.id = id;
        this.type = type;
        this.name = name;
        this.version = 0L;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public EventType getType() {
        return type;
    }

    @Override
    public boolean equals(Object target) {
        //
        if (this == target) {
            return true;
        }

        if (target == null || getClass() != target.getClass()) {
            return false;
        }

        NaraEvent entity = (NaraEvent) target;

        return Objects.equals(id, entity.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public static void main(String[] args) {
        //
        System.out.println(UUID.randomUUID().toString());
        System.out.println(UUID.randomUUID().toString());
    }
}
