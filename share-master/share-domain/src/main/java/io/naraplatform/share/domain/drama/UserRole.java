package io.naraplatform.share.domain.drama;

import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class UserRole implements JsonSerializable {
    //
    private String name;
    private RoleLevel level;

    public UserRole(String name) {
        //
        this(name, RoleLevel.Normal);
    }

    public UserRole(String name, RoleLevel level) {
        //
        this.name = name;
        this.level = level;
    }

    @Override
    public boolean equals(Object target) {
        //
        if (this == target) {
            return true;
        }

        if (target == null || getClass() != target.getClass()) {
            return false;
        }

        UserRole entity = (UserRole) target;

        return Objects.equals(name, entity.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public UserRole copy() {
        //
        return new UserRole(name, level);
    }

    public static UserRole sample() {
        //
        return new UserRole("Instructor", RoleLevel.Admin);
    }

    public static UserRole fromJson(String json) {
        //
        return JsonUtil.fromJson(json, UserRole.class);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
