package io.naraplatform.share.domain.enumtype;

public enum SortOrder {
    //
    Ascending,
    Descending
}
