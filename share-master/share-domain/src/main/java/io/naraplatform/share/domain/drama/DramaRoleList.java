package io.naraplatform.share.domain.drama;

import io.naraplatform.share.domain.ValueObject;
import io.naraplatform.share.util.json.JsonUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class DramaRoleList implements ValueObject {
    //
    private List<DramaRole> roles;

    public DramaRoleList() {
        //
        this.roles = new ArrayList<>();
    }

    public DramaRoleList(DramaRole role) {
        //
        this();
        this.roles.add(role);
    }

    public DramaRoleList(List<DramaRole> roles) {
        //
        this();
        this.roles.addAll(roles);
    }

    public static List<DramaRole> defaultRoles() {
        //
        List<DramaRole> defaultRoles = new ArrayList<>();
        defaultRoles.add(DramaRole.userRole());
        defaultRoles.add(DramaRole.adminRole());

        return defaultRoles;
    }

    public static DramaRoleList userRole() {
        //
        return new DramaRoleList(DramaRole.userRole());
    }

    public static DramaRoleList adminRole() {
        //
        return new DramaRoleList(DramaRole.adminRole());
    }

    public String toString() {
        //
        return toJson();
    }

    public static DramaRoleList fromJson(String json) {
        //
        return JsonUtil.fromJson(json, DramaRoleList.class);
    }

    public static DramaRoleList sample() {
        //
        DramaRoleList sample = new DramaRoleList(DramaRole.adminSample());
        sample.add(DramaRole.userRole());

        return sample;
    }

    public int size() {
        //
        return roles.size();
    }

    public List<DramaRole> getRoles() {
        return roles;
    }

    public boolean hasRole() {
        //
        if(size() > 0) {
            return true;
        }

        return false;
    }

    public DramaRole getOne(int index) {
        //
        if(!hasRole()) {
            throw new NoSuchElementException("Role index: " + index);
        }

        return roles.get(index);
    }

    public void add(DramaRole role) {
        //
        roles.add(role);
    }

    public void addAll(List<DramaRole> roles) {
        //
        roles.addAll(roles);
    }

    public DramaRole getByName(String roleName) {
        //
        for(DramaRole role : this.roles) {
            for(String name : role.getNames().getStrings()) {
                if (name.equals(roleName)) {
                    return role;
                }
            }
        }

        throw new NoSuchElementException("Role name: " + roleName);
    }

    public boolean hasByName(String roleName) {
        //
        try {
            getByName(roleName);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
}
