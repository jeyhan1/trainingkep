package io.naraplatform.share.domain.granule;

import io.naraplatform.share.domain.ValueObject;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Guest implements ValueObject {
    //
    private String id;                  // can be null.
    private String displayName;
    private String email;

    public Guest(String id, String name, String email) {
        //
        this.id = id;
        this.displayName = name;
        this.email = email;
    }

    public Guest(String name, String email) {
        //
        this(null, name, email);
    }

    public String toString() {
        //
        return toJson();
    }

    public static Guest fromJson(String json) {
        //
        return JsonUtil.fromJson(json, Guest.class);
    }

    public static Guest sample() {
        //
        return new Guest("Steve Jobs", "steve@gmail.com");
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
