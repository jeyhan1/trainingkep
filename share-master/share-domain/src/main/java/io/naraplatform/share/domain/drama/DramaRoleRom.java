package io.naraplatform.share.domain.drama;

import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DramaRoleRom implements JsonSerializable {
    //
    private String lang;
    private int index;
    private String key;
    private String name;
    private RoleLevel level;

    public DramaRoleRom(String lang, DramaRole role) {
        //
        this.lang = lang;
        this.index = role.getIndex();
        this.key = role.getKey();
        this.name = role.getNames().getString(lang);
        this.level = role.getLevel();
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static DramaRoleRom fromJson(String json) {
        //
        return JsonUtil.fromJson(json, DramaRoleRom.class);
    }

    public static DramaRoleRom adminSample() {
        //
        return new DramaRoleRom("en", DramaRole.adminSample());
    }

    public static DramaRoleRom koAdminSample() {
        //
        return new DramaRoleRom("ko", DramaRole.adminSample());
    }

    public static void main(String[] args) {
        //
        System.out.println(adminSample());
        System.out.println(koAdminSample());
    }
}
