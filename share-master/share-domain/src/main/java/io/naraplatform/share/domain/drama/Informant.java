package io.naraplatform.share.domain.drama;

import io.naraplatform.share.domain.IdName;
import io.naraplatform.share.domain.ValueObject;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Informant implements ValueObject {
    //
    private String cineroomId;
    private String playerId;
    private IdName sourceEntity;

    public Informant(String cineroomId, String playerId, IdName sourceEntity) {
        //
        this.cineroomId = cineroomId;
        this.playerId = playerId;
        this.sourceEntity = sourceEntity;
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static Informant sample() {
        //
        String cineroomId = "M1S1P1C1";
        String playerId = "2@M1S1P1C1";
        IdName sourceEntity = new IdName("nara.drama.Board", UUID.randomUUID().toString());

        Informant sample = new Informant(cineroomId, playerId, sourceEntity);

        return sample;
    }

    public static Informant fromJson(String json) {
        //
        return JsonUtil.fromJson(json, Informant.class);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
