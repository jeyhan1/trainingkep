package io.naraplatform.share.domain;

import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;
import java.util.StringTokenizer;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class IdKeyName implements ValueObject {
    //
    private String id;
    private String key;
    private String name;

    public IdKeyName(String id, String key, String name) {
        //
        this.id = id;
        this.key = key;
        this.name = name;
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public String toSimpleString() {
        //
        return id + ":" + key + ":" + name;
    }

    public static IdKeyName fromString(String idNameStr) {
        //
        StringTokenizer tokenizer = new StringTokenizer(idNameStr, ":");
        String id = tokenizer.nextToken();
        String key = tokenizer.nextToken();
        String name = tokenizer.nextToken();

        return new IdKeyName(id, key, name);
    }

    public static IdKeyName sample() {
        //
        String id = UUID.randomUUID().toString();
        String key = "nextree";
        String name = "Nextree Ltd.";

        return new IdKeyName(id, key, name);
    }

    public static IdKeyName fromJson(String json) {
        //
        return JsonUtil.fromJson(json, IdKeyName.class);
    }

    @Override
    public boolean equals(Object target) {
        //
        if (this == target) {
            return true;
        }

        if (target == null || getClass() != target.getClass()) {
            return false;
        }

        IdKeyName idName = (IdKeyName) target;

        return Objects.equals(id, idName.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
        System.out.println(IdKeyName.fromString(sample().toSimpleString()));
    }
}
