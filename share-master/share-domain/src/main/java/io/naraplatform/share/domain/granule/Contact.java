package io.naraplatform.share.domain.granule;

import io.naraplatform.share.domain.NameValueList;
import io.naraplatform.share.domain.ValueObject;
import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Contact implements JsonSerializable {
    //
    private String email;
    private String phone;
    private Address address;        // Optional
    private NameValueList additionalContacts;       // telegram id, skype id, etc

    public Contact(String email, String phone) {
        //
        this.email = email;
        this.phone = phone;
        this.address = null;
        this.additionalContacts = new NameValueList();
    }

    public String toString() {
        //
        return toJson();
    }

    public static Contact newEmailPhone(String email, String phone) {
        //
        return new Contact(email, phone);
    }

    public static Contact sample() {
        //
        String email = Email.sample().getEmail();
        String phone = Phone.mobileSample().getFullNumber();

        Contact sample = new Contact(email, phone);
        sample.setAddress(Address.usSample());

        return sample;
    }

    public static Contact fromJson(String json) {
        //
        return JsonUtil.fromJson(json, Contact.class);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
