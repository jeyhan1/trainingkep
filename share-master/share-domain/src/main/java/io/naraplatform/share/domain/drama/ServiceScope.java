package io.naraplatform.share.domain.drama;

public enum ServiceScope {
    //
    Service,
    Org,
    Team,
    User;
}
