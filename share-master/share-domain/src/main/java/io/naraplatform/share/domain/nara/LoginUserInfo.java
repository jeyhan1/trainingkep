package io.naraplatform.share.domain.nara;

import io.naraplatform.share.domain.ValueObject;
import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import io.naraplatform.share.util.security.bcrypt.BCryptPasswordEncoder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class LoginUserInfo implements ValueObject, JsonSerializable {
    //
    private String displayName;
    private String loginId;
    private String encryptedPassword;
    private LoginIdType idType;
    private WorkspaceType workspaceType;
    private Long time;

    private LoginUserInfo(String displayName, String loginId) {
        //
        this.displayName = displayName;
        this.loginId = loginId;
        this.idType = LoginIdType.Email;
        this.workspaceType = WorkspaceType.Cineroom;
        this.time = System.currentTimeMillis();
    }

    public static LoginUserInfo newWithNoEncrypted(String displayName,
                                                   String loginId,
                                                   String password) {
        //
        LoginUserInfo loginUserInfo = new LoginUserInfo(displayName, loginId);
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encryptedPassword = encoder.encode(password);
        loginUserInfo.setEncryptedPassword(encryptedPassword);

        return loginUserInfo;
    }

    public String toString() {
        //
        return toJson();
    }

    public static LoginUserInfo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, LoginUserInfo.class);
    }

    public static LoginUserInfo newStationInstance(String displayName,
                                                   String email,
                                                   String password){
        //
        LoginUserInfo loginUserInfo = newWithNoEncrypted(displayName, email, password);
        loginUserInfo.setWorkspaceType(WorkspaceType.Station);

        return loginUserInfo;
    }

    public static LoginUserInfo sample() {
        //
        String displayName = "Steve Jobs";
        String password = "12345";
        String userId = "steve@google.com";

        return newWithNoEncrypted(displayName, userId, password);
    }

    public boolean valid(String password) {
        //
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.check(password, encryptedPassword);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().valid("12345"));
    }
}