package io.naraplatform.share.domain.drama;

import io.naraplatform.share.domain.lang.LangString;
import io.naraplatform.share.domain.lang.LangStrings;
import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class TownRole implements JsonSerializable {
    //
    private int index;
    private String key;                 // ADMIN
    private LangStrings langNames;
    private RoleLevel level;
    private boolean defaultUserRole;

    public TownRole(int index,
                    String key,
                    LangStrings langNames,
                    RoleLevel level) {
        //
        this.index = index;
        this.key = key.toUpperCase();
        this.langNames = langNames;
        this.level = level;
        this.defaultUserRole = false;
    }

    public TownRole(int index,
                    String key,
                    LangString langName,
                    RoleLevel level) {
        //
        this(index,key, LangStrings.newString(langName),level);
    }

    public static List<TownRole> defaultRoles() {
        //
        return Arrays.asList(defaultUserRole(), adminRole());
    }

    public static TownRole defaultUserRole() {
        //
        TownRole role = new TownRole(
            0,
            "USER",
            LangStrings.newString("en","User").addString("ko", "사용자"),
            RoleLevel.Normal);
        role.setDefaultUserRole(true);

        return role;
    }

    public static TownRole adminRole() {
        //
        return new TownRole(
            1,
            "ADMIN",
            LangStrings.newString("en", "Admin").addString("ko", "관리자"),
            RoleLevel.Admin);
    }

    @Override
    public boolean equals(Object target) {
        //
        if (this == target) {
            return true;
        }

        if (target == null || getClass() != target.getClass()) {
            return false;
        }

        TownRole entity = (TownRole) target;

        return Objects.equals(key, entity.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public TownRole copy() {
        //
        return new TownRole(index, key, langNames, level);
    }

    public static TownRole sample() {
        //
        return new TownRole(
            0,
            "ADMIN",
            LangStrings.newString("en", "Instructor").addString("ko", "강사"),
            RoleLevel.Admin);
    }

    public static TownRole fromJson(String json) {
        //
        return JsonUtil.fromJson(json, TownRole.class);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
