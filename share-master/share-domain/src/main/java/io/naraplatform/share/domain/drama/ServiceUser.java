package io.naraplatform.share.domain.drama;

import io.naraplatform.share.domain.IdName;
import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ServiceUser implements JsonSerializable {
    //
    private IdName service;     // drama or ...
    private IdName org;         // square or ...
    private IdName team;        // cineroom or ...
    private IdName user;        // citizen or employee id or ...

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static ServiceUser fromJson(String json) {
        //
        return JsonUtil.fromJson(json, ServiceUser.class);
    }

    public static ServiceUser sample() {
        //
        IdName service = new IdName(UUID.randomUUID().toString(), "nara-board");
        IdName org = new IdName(UUID.randomUUID().toString(), "nextree consulting");
        IdName team = new IdName(UUID.randomUUID().toString(), "k-project team A");
        IdName user = new IdName(UUID.randomUUID().toString(), "Hong, Gildong");

        return new ServiceUser(service, org, team, user);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
