package io.naraplatform.share.domain.granule;

import io.naraplatform.share.domain.enumtype.Tier;
import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Email implements JsonSerializable {
    //
    private boolean official;
    private Tier tier;
    private String email;

    public Email(String email) {
        //
        this.tier = Tier.Primary;
        this.email = email;
        this.official = false;
    }

    public static Email newOfficialPrimary(String email) {
        //
        Email officialEmail = new Email(email);
        officialEmail.setOfficial(true);
        officialEmail.setTier(Tier.Primary);

        return officialEmail;
    }

    public static Email newOfficialSecondary(String email) {
        //
        Email officialEmail = new Email(email);
        officialEmail.setOfficial(true);
        officialEmail.setTier(Tier.Secondary);

        return officialEmail;
    }

    public static Email newPersonalSecondary(String email) {
        //
        Email officialEmail = new Email(email);
        officialEmail.setOfficial(true);
        officialEmail.setTier(Tier.Secondary);

        return officialEmail;
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static Email fromJson(String json) {
        return JsonUtil.fromJson(json, Email.class);
    }

    public static Email sample() {
        //
        String email = "jhone@company.com";
        return newOfficialPrimary(email);
    }

    public boolean isPrimary() {
        //
        if (this.tier.isPrimary()) {
            return true;
        }

        return false;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
