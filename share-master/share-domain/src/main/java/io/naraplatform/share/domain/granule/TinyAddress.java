package io.naraplatform.share.domain.granule;

import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TinyAddress implements JsonSerializable {
    //
    private String zipCode;
    private String zipAddress;
    private String street;
    private String coutry;

    public TinyAddress(String zipCode,
                       String zipAddress,
                       String street,
                       String country) {
        //
        this.zipCode = zipCode;
        this.zipAddress = zipAddress;
        this.street = street;
        this.coutry = country;
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static TinyAddress sample() {
        //
        String zipCode = "08876";
        String zipAddress = "Fairfield, CT";
        String street = "16th Berry street";
        String country = "U.S.A";

        return new TinyAddress(zipCode, zipAddress, street, country);
    }

    public static TinyAddress fromJson(String json) {
        //
        return JsonUtil.fromJson(json, TinyAddress.class);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
