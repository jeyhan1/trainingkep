package io.naraplatform.share.domain.granule;

import io.naraplatform.share.domain.ValueObject;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class AttachedFile implements ValueObject {
    //
    private String attachmentId;
    private String fileName;
    private String contentType;         // http Content-Type
    private Long contentLength;         // byte

    public AttachedFile(String attachmentId, String fileName) {
        //
        this.attachmentId = attachmentId;
        this.fileName = fileName;
    }

    public AttachedFile(String attachmentId, String fileName, String contentType, long contentLength) {
        //
        this.attachmentId = attachmentId;
        this.fileName = fileName;
        this.contentType = contentType;
        this.contentLength = contentLength;
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static AttachedFile fromJson(String json) {
        //
        return JsonUtil.fromJson(json, AttachedFile.class);
    }

    public static AttachedFile sample() {
        //
        String attachmentId = UUID.randomUUID().toString();
        String fileName = "Hello.java";
        AttachedFile sample = new AttachedFile(attachmentId, fileName);
        sample.setContentType("text/plain");
        sample.setContentLength(1004L);

        return sample;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
