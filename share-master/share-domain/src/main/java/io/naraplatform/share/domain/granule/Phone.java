package io.naraplatform.share.domain.granule;

import io.naraplatform.share.domain.ValueObject;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Phone implements ValueObject {
	//
	private Category category;

	private String countryCode;
	private String carrierCode;

	private String frontNumber;
	private String backNumber;

	private String fullNumber; 			// 91150909, 34569878,
	private String displayNumber; 		// +82 10-9112-0909

	public Phone(String countryCode, String carrierCode, String fullNumber) {
		//
		this(
				countryCode,
				carrierCode,
				fullNumber.substring(0, fullNumber.length()-4),
				fullNumber.substring(fullNumber.length()-4)
		);
	}

	public Phone(String countryCode, String carrierCode, String frontNumber, String backNumber) {
		//
		this.category = Category.Mobile;

		this.countryCode = countryCode;
		this.carrierCode = carrierCode.startsWith("0") ? carrierCode.substring(1) : carrierCode;
		this.frontNumber = frontNumber;
		this.backNumber = backNumber;
		this.fullNumber = frontNumber + backNumber;
		this.displayNumber = String.format("+%s %s-%s-%s", countryCode, carrierCode, frontNumber, backNumber);
	}

	@Override
	public String toString() {
		//
		return toJson();
	}

	public static Phone mobileSample() {
		//
		String countryCode = "82";
		String carrierCode = "010";
		String frontNumber = "1235";
		String backNumber = "3903";

		Phone sample = new Phone(countryCode, carrierCode, frontNumber, backNumber);
		sample.setCategory(Category.Mobile);

		return sample;
	}

	public static Phone officeSample() {
		//
		String countryCode = "82";
		String carrierCode = "02";
		String frontNumber = "2225";
		String backNumber = "2323";

		Phone sample = new Phone(countryCode, carrierCode, frontNumber, backNumber);
		sample.setCategory(Category.Office);

		return sample;
	}

	public static Phone fromJson(String json) {
		//
		return JsonUtil.fromJson(json, Phone.class);
	}

	public String toNumberOnly() {
		//
		return String.format("%s%s%s%s", countryCode, carrierCode, frontNumber, backNumber);
	}

	public enum Category {
		//
		Mobile,
		Office,
		Home,
		Others
	}

	public static void main(String[] args) {
		//
		System.out.println(mobileSample().toNumberOnly());
		System.out.println(mobileSample().toJson());
		System.out.println(fromJson(mobileSample().toJson()));
	}
}
