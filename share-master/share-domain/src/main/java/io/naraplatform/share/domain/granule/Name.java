package io.naraplatform.share.domain.granule;

import io.naraplatform.share.domain.ValueObject;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Name implements ValueObject {
	//
	private String title;               // Mr. Mrs. Ms.
    private String firstName;
    private String middleName;
    private String familyName;
    private boolean familyFirst;

    public Name(String firstName, String familyName) {
        //
        this.firstName = firstName;
        this.familyName = familyName;
        this.familyFirst = false;
    }

    public Name(String firstName, String middleName, String familyName) {
        //
        this.firstName = firstName;
        this.middleName  = middleName;
        this.familyName = familyName;
        this.familyFirst = false;
    }

    public static Name newGivenFirst(String givenName, String familyName) {
        //
        return new Name(givenName, familyName);
    }

    public static Name newFamilyFirst(String familyName, String givenName) {
        //
        Name name = new Name(givenName, familyName);
        name.setFamilyFirst(true);

        return name;
    }

    @Override
	public String toString() {
		//
		return toJson();
	}

    public String showFamilyFirst() {
        //
        return familyName + " " + firstName;
    }

    public String showFamilyLast() {
        //
        return firstName + " " + familyName;
    }

    public String getDisplayName() {
        //
        if(familyFirst) {
            return showFamilyFirst();
        } else {
            return showFamilyLast();
        }
    }

    public static Name sample() {
		//
		String firstName = "Steve";
		String familyName = "Jobs";

		Name sample = new Name(firstName, familyName);

		return sample;
	}

    public static Name sampleFamilyFirst() {
        //
        String firstName = "태진";
        String familyName = "김";

        Name sample = new Name(firstName, familyName);
        sample.setFamilyFirst(true);

        return sample;
    }

    public static Name fromJson(String json) {
		//
		return JsonUtil.fromJson(json, Name.class);
	}

	public static void main(String[] args) {
		//
		System.out.println(sample().toJson());
		System.out.println(sample().getDisplayName());
		System.out.println(sampleFamilyFirst().getDisplayName());
	}
}
