package io.naraplatform.share.domain.lang;

import io.naraplatform.share.domain.ValueObject;
import io.naraplatform.share.domain.granule.Address;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Locale;

@Getter
@Setter
@NoArgsConstructor
public class LangAddress implements ValueObject {
    //
    private String lang;
    private Address address;

    public LangAddress(String lang, Address address) {
        //
        this.lang = lang;
        this.address = address;
    }

    public LangAddress(Locale locale, Address address) {
        //
        this.lang = locale.getLanguage();
        this.address = address;
    }

    public static LangAddress sample() {
        //
        Locale locale = Locale.US;
        Address address = Address.usSample();

        LangAddress sample = new LangAddress(locale, address);

        return sample;
    }

    public static LangAddress secondSample() {
        //
        Locale langLocale = Locale.KOREA;
        Address address = Address.koreanSample();

        LangAddress sample = new LangAddress(langLocale, address);

        return sample;
    }

    public static LangAddress fromJson(String json) {
        //
        return JsonUtil.fromJson(json, LangAddress.class);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toJson());
        System.out.println(secondSample().toJson());
    }
}
