package io.naraplatform.share.domain.lang;

import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;

import java.util.ArrayList;
import java.util.List;

public class LangTextList implements JsonSerializable {
	//
	private List<LangText> langTexts;

	public LangTextList() {
		//
		this.langTexts = new ArrayList<>();
	}

	public LangTextList(LangText langText) {
		//
		this();
		this.langTexts.add(langText);
	}

	public LangTextList(String langCode, String text) {
		//
		this();
		this.langTexts.add(new LangText(langCode, text));
	}

	public LangTextList(List<LangText> langTexts) {
		//
		this.langTexts = langTexts;
	}

	@Override
	public String toString() {
		//
		return toJson();
	}

	public String toString(String langCode) {
	    //
        StringBuilder builder = new StringBuilder();
        boolean start = true;
        for(LangText text : langTexts) {
            if(start) {
                start = false;
            } else {
                builder.append(", ");
            }
            builder.append(text.getText(langCode));
        }

        return builder.toString();
    }

	public static LangTextList fromJson(String json) {
		//
		return JsonUtil.fromJson(json, LangTextList.class);
	}

	public LangTextList add(LangText langText) {
		//
		this.langTexts.add(langText);
		return this;
	}

	public void add(String langCode, String text) {
		//
		this.langTexts.add(new LangText(langCode, text));
	}

	public void addAll(List<LangText> langTexts) {
		//
		this.langTexts.addAll(langTexts);
	}

	public LangText get(int index) {
	    //
        if (langTexts.size() <= (index+1)) {
            return langTexts.get(index);
        }

        return null;
    }

	public List<LangText> list() {
		//
		return langTexts;
	}

	public void setList(List<LangText> langTexts) {
	    //
        this.langTexts = langTexts;
    }

	public boolean containsText(String text) {
		//
        return langTexts
            .stream()
            .anyMatch(lt -> lt.getText().equals(text));
	}

	public int size() {
		return langTexts.size();
	}

	public static LangTextList sample() {
	    //
        LangTextList textList = new LangTextList();
        textList.add(LangText.newText("en", "Tennis").addText("ko", "테니스"));
        textList.add(LangText.newText("en", "Soccer").addText("ko", "축구"));

        return textList;
    }

    public static void main(String[] args) {
	    //
        System.out.println(sample());
        System.out.println(sample().toString("en"));
        System.out.println(sample().toString("ko"));
    }
}
