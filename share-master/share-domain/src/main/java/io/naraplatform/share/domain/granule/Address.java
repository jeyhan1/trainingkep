package io.naraplatform.share.domain.granule;

import io.naraplatform.share.domain.ValueObject;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Address implements ValueObject {
    //
    private Category category;

    private String zipCode;
    private String zipAddress;

    private String city;
    private String state;

    private String street;              // non-zip addresss, or street address
    private String country;

    public Address(Category category, String zipCode, String country) {
        //
        this.zipCode = zipCode;
        this.country = country;
        this.category = category;
    }

    public static Address forKorean(Category category,
                                    String zipCode,
                                    String zipAddress,
                                    String street,
                                    String country) {
        //
        Address address = new Address(category, zipCode, country);
        address.setZipAddress(zipAddress);
        address.setStreet(street);

        return address;
    }

    public static Address forUS(Category category,
                                String zipCode,
                                String street,
                                String city,
                                String state,
                                String country) {
        //
        Address address = new Address(category, zipCode, country);
        address.setStreet(street);
        address.setCity(city);
        address.setState(state);

        return address;
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public TinyAddress toTinyAddress(String langCode) {
        //
        return new TinyAddress(zipCode, zipAddress, street, country);
    }

    public static Address usSample() {
        //
        Category category = Category.Office;
        String zipCode = "06889";
        String street = "12 Jhones st.";
        String city = "fairfield";
        String state = "CT";
        String country = "U.S.A";

        Address sample = Address.forUS(category, zipCode, street, city, state,  country);

        return sample;
    }

    public static Address koreanSample() {
        //
        Category category = Category.Office;
        String zipCode = "12345";
        String zipAddress = "서울시 금천구 디지털1로 155번지 잼잼빌딩";
        String street = "703호";
        String country = "대한민국";

        Address sample = Address.forKorean(category, zipCode, zipAddress, street, country);

        return sample;
    }

    public static Address fromJson(String json) {
        //
        return JsonUtil.fromJson(json, Address.class);
    }

    public enum Category {
        Office,
        Home,
        Others
    }

    public static void main(String[] args) {
        //
        System.out.println(koreanSample());
        System.out.println(usSample().toTinyAddress("en"));
        System.out.println(fromJson(usSample().toJson()));
    }
}
