package io.naraplatform.share.domain.granule;

import io.naraplatform.share.domain.lang.LangName;
import io.naraplatform.share.domain.lang.LangNames;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Locale;

@Getter
@Setter
@NoArgsConstructor
public class OrgContact extends Contact {
    //
    private String fax;
    private LangNames representativeNames;

    public OrgContact(LangName representativeName,
                      String email,
                      String phone) {
        //
        super(email,phone);
        this.representativeNames = LangNames.newName(representativeName);
    }

    public String toString() {
        //
        return toJson();
    }

    public static OrgContact fromJson(String json) {
        //
        return JsonUtil.fromJson(json, OrgContact.class);
    }

    public static OrgContact sample() {
        //
        LangName representativeName = new LangName(
            Locale.KOREA.getLanguage(),
            new Name("Minsoo", "Kim"));
        String email = Email.sample().getEmail();
        String phone = Phone.mobileSample().getDisplayNumber();

        return new OrgContact(representativeName, email, phone);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
