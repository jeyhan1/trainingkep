package io.naraplatform.share.domain.lang;

import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.*;

@Getter
@Setter
@NoArgsConstructor
public class LangLabels implements JsonSerializable {
    //
    private String defaultLanguage;                // English
    private Map<String, String> langLabelMap;            // langCode : label text

    private LangLabels(String defaultLanguage, String defaultLabel) {
        //
        this.defaultLanguage = defaultLanguage;
        this.langLabelMap = new HashMap<>();
        this.langLabelMap.put(defaultLanguage, defaultLabel);
    }

    public String toString() {
        //
        return toJson();
    }

    public static LangLabels fromJson(String json) {
        //
        return JsonUtil.fromJson(json, LangLabels.class);
    }

    public static LangLabels newLabel(String defaultLanguage, String defaultLabel) {
        //
        LangLabels langLabels = new LangLabels(defaultLanguage, defaultLabel);
        return langLabels;
    }

    public LangLabels addLabel(String langCode, String label) {
        //
        this.langLabelMap.put(langCode, label);
        return this;
    }

    public int countLabel() {
        //
        return (this.langLabelMap.size());
    }

    public String getLabel(String langCode) {
        //
        return langLabelMap.get(langCode);
    }

    public void removeLabel(String langCode) {
        //
        langLabelMap.remove(langCode);
    }

    public static LangLabels sample() {
        //
        String defaultLabel = "Student";
        LangLabels langLabels = LangLabels.newLabel(Locale.US.getLanguage(), defaultLabel);
        langLabels.addLabel(Locale.KOREA.getLanguage(), "학생");

        return langLabels;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
