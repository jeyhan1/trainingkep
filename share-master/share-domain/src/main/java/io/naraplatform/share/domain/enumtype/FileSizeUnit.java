package io.naraplatform.share.domain.enumtype;

public enum FileSizeUnit {
	
    //
    GB(SizeUnit.SIZE_UNIT_GB),
    MB(SizeUnit.SIZE_UNIT_MB),
    KB(SizeUnit.SIZE_UNIT_KB),
    Byte(SizeUnit.SIZE_UNIT_B);

    private String fullName;
    
    private FileSizeUnit(String fullName) {
        //
        this.fullName = fullName;
    }

    public String fullName() {
        //
        return fullName;
    }
    
    private static class SizeUnit {
    	public static final String SIZE_UNIT_GB = "GigaByte";
    	public static final String SIZE_UNIT_MB = "MegaByte";
    	public static final String SIZE_UNIT_KB = "KiloByte";
    	public static final String SIZE_UNIT_B = "Byte";
    }
    
}
