package io.naraplatform.share.domain.granule;

import io.naraplatform.share.domain.enumtype.Tier;
import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;

import java.util.ArrayList;
import java.util.List;

public class AdminList implements JsonSerializable {
    ////
    private List<Admin> admins;

    public AdminList() {
        //
        this.admins = new ArrayList<>();
    }

    public AdminList(Admin admin) {
        //
        this();
        this.admins.add(admin);
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static AdminList sample() {
        //
        AdminList sample = new AdminList(Admin.primarySample());
        sample.add(Admin.secondarySample());

        return sample;
    }

    public static AdminList fromJson(String json) {
        //
        return JsonUtil.fromJson(json, AdminList.class);
    }

    public void add(Admin admin) {
        //
        admins.add(admin);
    }

    public void addAll(List<Admin> admins) {
        //
        admins.addAll(admins);
    }

    public List<Admin> list() {
        //
        return admins;
    }

    public Admin find(String usid) {
        //
        return admins.stream().filter(admin -> usid.equals(admin.getUsid())).findFirst().orElse(null);
    }

    public void remove(String usid) {
        //
        Admin admin = find(usid);
        if (admin == null) {
            return;
        }

        admins.remove(admin);
    }

    public boolean contains(String id) {
        //
        return find(id) != null;
    }

    public Admin findPrimary() {
        //
        for (Admin admin : this.admins) {
            if (Tier.Primary == admin.getTier()) {
                return admin;
            }
        }
        return null;
    }

    public int size() {
        //
        return admins.size();
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
        System.out.println(sample().toJson());
        System.out.println(fromJson(sample().toJson()));
    }
}
