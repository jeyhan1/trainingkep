package io.naraplatform.share.domain.granule;

import io.naraplatform.share.domain.ValueObject;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Host implements ValueObject {
    //
    private String id;                  // can be null.
    private String displayName;
    private String email;

    public Host(String id, String name, String email) {
        //
        this.id = id;
        this.displayName = name;
        this.email = email;
    }

    public Host(String name, String email) {
        //
        this(null, name, email);
    }

    public String toString() {
        //
        return toJson();
    }

    public static Host fromJson(String json) {
        //
        return JsonUtil.fromJson(json, Host.class);
    }

    public static Host sample() {
        //
        return new Host("Tim cook", "timcook@gmail.com");
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
