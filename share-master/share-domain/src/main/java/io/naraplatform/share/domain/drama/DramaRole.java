package io.naraplatform.share.domain.drama;

import io.naraplatform.share.domain.lang.LangStrings;
import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DramaRole implements JsonSerializable {
    //
    private int index;
    private String key;
    private LangStrings names;
    private RoleLevel level;

    public DramaRole(int index, LangStrings names, RoleLevel level) {
        //
        this.index = index;
        this.key = names.getString(names.firstLang());
        this.names = names;
        this.level = level;
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static DramaRole fromJson(String json) {
        //
        return JsonUtil.fromJson(json, DramaRole.class);
    }

    public static DramaRole userRole() {
        //
        return new DramaRole(0, LangStrings.newString("en","User"), RoleLevel.Normal);
    }

    public static DramaRole adminRole() {
        //
        return new DramaRole(1, LangStrings.newString("en", "Admin"), RoleLevel.Admin);
    }

    public static DramaRole adminSample() {
        //
        return new DramaRole(0, LangStrings.newString("en", "Admin").addString("ko", "어드민"), RoleLevel.Admin);
    }

    public static DramaRole userSample() {
        //
        return new DramaRole(1, LangStrings.newString("en", "User").addString("ko", "사용자"), RoleLevel.Normal);
    }

    public static void main(String[] args) {
        //
        System.out.println(adminSample());
    }
}
