package io.naraplatform.share.domain.granule;

import io.naraplatform.share.domain.ValueObject;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class EmailMessage implements ValueObject {
    //
    private String from;
    private String[] to;
    private String[] cc;
    private Date sentDate;
    private String subject;
    private String text;
    private String[] attachments;

    public EmailMessage(String to, String subject, String text) {
        //
        super();
        this.to = new String[] {to};
        this.subject = subject;
        this.text = text;
    }

    public String toString() {
        //
        return toJson();
    }

    public static EmailMessage fromJson(String json) {
        //
        return JsonUtil.fromJson(json, EmailMessage.class);
    }

    public static EmailMessage sample() {
        //
        EmailMessage message = new EmailMessage(
                "id@nextree.co.kr"
                , "Greetings from Crunchify.."
                , "Test email by Crunchify.com JavaMail API example. " + "<br><br> Regards, <br>Crunchify Admin"
        );

        return message;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
