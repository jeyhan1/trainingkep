package io.naraplatform.share.domain;

import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;
import java.util.StringTokenizer;

@Getter
@Setter
@NoArgsConstructor
public class UsidName implements ValueObject {
    //
    private String usid;
    private String name;

    public UsidName(String usid, String name) {
        //
        this.usid = usid;
        this.name = name;
    }

    public String toSimpleString() {
        //
        return usid + ":" + name;
    }

    public static UsidName fromString(String idNameStr) {
        //
        StringTokenizer tokenizer = new StringTokenizer(idNameStr, ":");
        String id = tokenizer.nextToken();
        String name = tokenizer.nextToken();

        return new UsidName(id, name);
    }

    public static UsidName sample() {
        //
        String id = "1234";
        String name = "Hansoo Lee";

        return new UsidName(id, name);
    }

    public static UsidName fromJson(String json) {
        //
        return JsonUtil.fromJson(json, UsidName.class);
    }

    @Override
    public boolean equals(Object target) {
        //
        if (this == target) {
            return true;
        }

        if (target == null || getClass() != target.getClass()) {
            return false;
        }

        UsidName idName = (UsidName) target;

        return Objects.equals(usid, idName.usid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(usid);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
        System.out.println(UsidName.fromString(sample().toSimpleString()));
    }
}
