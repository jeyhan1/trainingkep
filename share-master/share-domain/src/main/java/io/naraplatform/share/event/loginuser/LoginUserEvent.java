package io.naraplatform.share.event.loginuser;

import io.naraplatform.share.domain.nara.LoginUserInfo;
import io.naraplatform.share.event.EventType;
import io.naraplatform.share.event.NaraEvent;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
@ToString
public class LoginUserEvent extends NaraEvent {
    //
    private String loginId;
    private LoginUserInfo loginUserInfo;

    public LoginUserEvent() { this(null, StringUtils.EMPTY); }

    public LoginUserEvent(EventType eventType, LoginUserInfo loginUserInfo) {
        //
        super(eventType, LoginUserEvent.class.getSimpleName());
        this.loginUserInfo = loginUserInfo;
    }

    private LoginUserEvent(EventType eventType, String loginId) {
        //
        super(eventType, LoginUserEvent.class.getSimpleName());
        this.loginId = loginId;
    }

    public LoginUserInfo getLoginUserInfo() {
        return loginUserInfo;
    }

    public static LoginUserEvent fromJson(String json) {
        //
        return JsonUtil.fromJson(json, LoginUserEvent.class);
    }

    public static LoginUserEvent getSample() {
        //
        LoginUserEvent sample = new LoginUserEvent(EventType.Created, LoginUserInfo.sample());

        return sample;
    }

    public static LoginUserEvent buildCreateEvent(LoginUserInfo loginUserInfo) {
        //
        return new LoginUserEvent(EventType.Created, loginUserInfo);
    }

    public static LoginUserEvent buildUpdateEvent(LoginUserInfo loginUserInfo) {
        //
        return new LoginUserEvent(EventType.Updated, loginUserInfo);
    }

    public static LoginUserEvent buildDeleteEvent(String loginId) {
        //
        return new LoginUserEvent(EventType.Deleted, loginId);
    }

    public static void main(String[] args) {
        //
        System.out.println(getSample());
    }
}
