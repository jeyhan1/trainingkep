package io.naraplatform.share.domain.lang;

import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Getter
@Setter
public class LangStrings implements JsonSerializable {
    //
    private List<LangString> langStrings;

    public LangStrings() {
        //
        this.langStrings = new ArrayList<>();
    }

    private LangStrings(String langCode, String text) {
        //
        this();
        this.langStrings.add(new LangString(langCode, text));
    }

    public static LangStrings emptyString() {
        //
        return new LangStrings();
    }

    public static LangStrings newString(String landCode, String string) {
        //
        return new LangStrings(landCode, string);
    }

    public static LangStrings newString(LangString langString) {
        //
        LangStrings newString = new LangStrings();
        newString.getLangStrings().add(langString);

        return newString;
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static LangStrings fromJson(String json) {
        //
        return JsonUtil.fromJson(json, LangStrings.class);
    }

    public static LangStrings sample() {
        //
        String langCode = Locale.KOREA.getLanguage();
        LangStrings sample = new LangStrings(langCode, "넥스트리소프트");
        sample.addString(Locale.US, "NEXTREE SOFT");

        return sample;
    }

    public String firstLang() {
        //
        if(langStrings == null || langStrings.size() == 0) {
            return null;
        }

        return langStrings.get(0).getLang();
    }

    public String firstString() {
        //
        if(langStrings == null || langStrings.size() == 0) {
            return null;
        }

        return langStrings.get(0).getString();
    }

    public int size() {
        //
        return langStrings.size();
    }

    public List<LangString> list() {
        //
        return this.langStrings;
    }

    public List<String> getLangs() {
        //
        List<String> langs = new ArrayList<>();
        for(LangString langString : langStrings) {
            langs.add(langString.getLang());
        }

        return langs;
    }

    public List<String> getStrings() {
        //
        List<String> strings = new ArrayList<>();
        for(LangString langString : langStrings) {
            strings.add(langString.getString());
        }

        return strings;
    }

    public String getString() {
        //
        return getString(firstLang());
    }

    public String getString(String langCode) {
        //
        String found = null;
        for(LangString langString : langStrings) {
            if(langString.getLang().equals(langCode)) {
                found = langString.getString();
                break;
            }
        }

        if(found == null) {
            return null;
        }

        return found;
    }

    public LangStrings addString(String langCode, String string) {
        //
        if(langCode == null || string == null) {
            return this;
        }
        langStrings.add(new LangString(langCode, string));

        return this;
    }

    public LangStrings addString(Locale locale, String string) {
        //
        return addString(locale.getLanguage(), string);

    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
        System.out.println(sample().getLangs());
        System.out.println(sample().getStrings());
    }
}
