package io.naraplatform.share.domain.lang;

import io.naraplatform.share.domain.granule.Name;
import io.naraplatform.share.util.json.JsonSerializable;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class LangNames implements JsonSerializable {
    //
    private List<LangName> names;

    public LangNames() {
        //
        this.names = new ArrayList<>();
    }

    private LangNames(LangName langName) {
        //
        this();
        this.names.add(langName);
    }

    public static LangNames newName(LangName langName) {
        //
        return new LangNames(langName);
    }

    public static LangNames newName(String language, Name name) {
        //
        return new LangNames(new LangName(language, name));
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static LangNames fromJson(String json) {
        //
        return JsonUtil.fromJson(json, LangNames.class);
    }

    public static LangNames sample() {
        //
        LangNames sample = new LangNames(LangName.sample());
        sample.add(LangName.secondSample());

        return sample;
    }

    public int size() {
        //
        return names.size();
    }

    public List<LangName> list() {
        //
        return this.names;
    }

    public LangName getFirst() {
        //
        if (names == null || names.size() == 0) {
            return null;
        }

        return names.get(0);
    }

    public String getFirstName() {
        //
        if (names == null || names.size() == 0) {
            return null;
        }

        return names.get(0).getName().getDisplayName();
    }

    public List<LangName> names() {
        //
        return this.names;
    }

    public Name get(String lang) {
        //
        LangName found = null;
        for(LangName langName : names) {
            if(langName.getLanguage().equals(lang)) {
                found = langName;
                break;
            }
        }

        if(found == null) {
            return null;
        }

        return found.getName();
    }

    public LangNames add(LangName langName) {
        //
        if(langName == null) {
            return this;
        }

        names.add(langName);

        return this;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
