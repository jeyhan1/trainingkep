package io.naraplatform.share.domain;

import io.naraplatform.share.domain.lang.LangStrings;
import io.naraplatform.share.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Locale;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class IdLangName implements ValueObject {
    //
    private String id;
    private LangStrings names;

    public IdLangName(String id, LangStrings names) {
        //
        this.id = id;
        this.names = names;
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static IdLangName sample() {
        //
        String id = "1234";
        LangStrings names = LangStrings.newString(Locale.US.getLanguage(), "Hansoo Lee");
        names.addString(Locale.KOREA, "이 한수");

        return new IdLangName(id, names);
    }

    public static IdLangName fromJson(String json) {
        //
        return JsonUtil.fromJson(json, IdLangName.class);
    }

    public IdName idName(String langCode) {
        //
        return new IdName(id, names.getString(langCode));
    }

    @Override
    public boolean equals(Object target) {
        //
        if (this == target) {
            return true;
        }

        if (target == null || getClass() != target.getClass()) {
            return false;
        }

        IdLangName idName = (IdLangName) target;

        return Objects.equals(id, idName.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
        System.out.println(sample().idName(Locale.KOREA.getLanguage()));
    }
}
